<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package leviosa
 */

$hide_body = get_post_meta( get_the_ID(), 'pp_hide_body', true );
$body_position = get_post_meta( get_the_ID(), 'pp_body_position', true );
if ( !$body_position || is_home() || is_front_page() ) $body_position = ot_get_option( 'body_position', 'center' );

get_header(); ?>


	<div id="primary" class="content-area <?php echo (is_home()) ? 'blog-page' : '' ?> <?php echo ot_get_option( 'blog_style', 'blog-standard' ) ?>">
		<main id="main" class="site-main <?php echo (isset($hide_body[0])) ? $hide_body[0] : ''; ?> <?php echo $body_position; ?>" role="main">
            <?php if ( have_posts() ) : ?>

            <?php if ( is_home() && ot_get_option( 'blog_style', 'blog-standard' ) === 'blog-standard' ) : ?>
            <div class="row collapse"><div class="one columns">&nbsp;</div><div class="eight columns">
            <?php endif; ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php leviosa_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

        <?php if ( is_home() && ot_get_option( 'blog_style', 'blog-standard' ) === 'blog-standard' ) : ?>
        </div>
        <div class="three columns">
            <div class="blog-sidebar"><?php dynamic_sidebar( 'blog-sidebar' ) ?></div>
        </div></div>
        <?php else : ?>
        <?php include('parts/toggle-icon.php'); ?>
        <?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
