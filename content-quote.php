<?php
$quote_name = get_post_meta( get_the_ID(), 'lv_quote_name', true );
$quote_src  = get_post_meta( get_the_ID(), 'lv_quote_src', true );
?>

<article id="post-<?php the_ID() ?>" <?php post_class( 'space-bottom-3' ) ?>>
    <a href="<?php echo esc_attr( $quote_src ) ?>" target="_blank">
        <div class="wrapper">
            <header class="entry-header">
                <h1 class="entry-title">
                    <?php echo get_the_content() ?>
                    <span class="icon-wrapper"><i class="levicon-quote-left"></i></span>
                </h1>
            </header>
            <div class="entry-content">
                <div class="quote-author"><?php echo $quote_name ?></div>
            </div>
        </div>
    </a>

    <?php if ( 'post' == get_post_type() ) : ?>
        <div class="entry-date-meta">
            <?php leviosa_posted_meta(); ?>
        </div><!-- .entry-meta -->
    <?php endif; ?>
</article>