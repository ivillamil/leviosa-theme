<?php
$link_title = get_post_meta( get_the_ID(), 'lv_link_title', true );
$link_src   = get_post_meta( get_the_ID(), 'lv_link_src', true );
?>

<article id="post-<?php the_ID() ?>" <?php post_class( 'space-bottom-3' ) ?>>
    <a href="<?php echo esc_attr( $link_src ) ?>" rel="bookmark" target="_blank">
        <div class="wrapper">
            <header class="entry-header">
                <h1 class="entry-title">
                    <span class="icon-wrapper"><i class="levicon-link"></i></span>
                    <?php echo $link_title ?>
                </h1>
            </header>
            <div class="entry-content">
                <div class="entry-content-url"><?php echo $link_src; ?></div>
            </div>
        </div>
    </a>

    <?php if ( 'post' == get_post_type() ) : ?>
    <div class="entry-date-meta">
        <?php leviosa_posted_meta(); ?>
    </div><!-- .entry-meta -->
    <?php endif; ?>
</article>