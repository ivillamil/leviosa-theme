<?php
/**
 * leviosa functions and definitions
 *
 * @package leviosa
 */

define( 'THEME_SLUG', 'leviosa' );
define( 'THEME_PATH', get_template_directory() );
define( 'THEME_URI',  get_template_directory_uri() );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'leviosa_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function leviosa_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on leviosa, use a find and replace
	 * to change 'leviosa' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'leviosa', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

    // Add default support for html5
    add_theme_support( 'html5' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary'   => __( 'Primary Menu', THEME_SLUG ),
        'footer'    => __( 'Footer Menu', THEME_SLUG )
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'leviosa_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

    // Array of Google Web Fonts
    $fonts = array(
        'serif'               => array( 'title' => 'serif',               'css' => '',                                 'styles' => array(),                     'weights' => array() ),
        'droid-serif'         => array( 'title' => 'Droid Serif',         'css' => '"Droid Serif", serif',             'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '700', '400italic', '700italic' ) ),
        'lora'                => array( 'title' => 'Lora',                'css' => '"Lora", serif',                    'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '700', '400italic', '700italic' ) ),
        'arvo'                => array( 'title' => 'Arvo',                'css' => '"Arvo", serif',                    'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '700', '400italic', '700italic' ) ),
        'bitter'              => array( 'title' => 'Bitter',              'css' => '"Bitter", serif',                  'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '400italic', '700' ) ),
        'merriweather'        => array( 'title' => 'Merriweather',        'css' => '"Merriweather", serif',            'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '300', '300italic', '400italic', '700', '700italic', '900', '900italic' ) ),
        'pt-serif'            => array( 'title' => 'PT Serif',            'css' => '"PT Serif", serif',                'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '700', '400italic', '700italic' ) ),
        'rokkitt'             => array( 'title' => 'Rokkitt',             'css' => '"Rokkitt", serif',                 'styles' => array( 'normal' ),           'weights' => array( '400', '700' ) ),
        'libre-baskerville'   => array( 'title' => 'Libre Baskerville',   'css' => '"Libre Baskerville", serif',       'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '700', '400italic' ) ),
        'roboto-slab'         => array( 'title' => 'Roboto Slab',         'css' => '"Roboto Slab", serif',             'styles' => array( 'normal' ),           'weights' => array( '400', '100', '300', '700' ) ),
        'playfair-display'    => array( 'title' => 'Playfair Display',    'css' => '"Playfair Display", serif',        'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '700', '900', '400italic', '700italic', '900italic' ) ),

        'open-sans'           => array( 'title' => 'Open Sans',           'css' => '"Open Sans", sans-serif',           'styles' => array( 'normal', 'italic' ), 'weights' => array( '300italic', '400italic', '600italic', '700italic', '800italic', '400', '300', '600', '700', '800' ) ),
        'roboto'              => array( 'title' => 'Roboto',              'css' => '"Roboto", sans-serif',              'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '100', '100italic', '300', '300italic', '500', '500italic','700', '700italic', '900', '900italic' ) ),
        'oswald'              => array( 'title' => 'Oswald',              'css' => '"Oswald", sans-serif',              'styles' => array( 'normal' ),           'weights' => array( '700', '300', '700' ) ),
        'droid-sans'          => array( 'title' => 'Droid Sans',          'css' => '"Droid Sans", sans-serif',          'styles' => array( 'normal' ),           'weights' => array( '400', '700' ) ),
        'lato'                => array( 'title' => 'Lato',                'css' => '"Lato", sans-serif',                'styles' => array( 'normal', 'italic' ), 'weights' => array( '300', '400', '700', '300italic', '400italic', '700italic' ) ),
        'open-sans-condensed' => array( 'title' => 'Open Sans Condensed', 'css' => '"Open Sans Condensed", sans-serif', 'styles' => array( 'normal' ),           'weights' => array( '300', '700' ) ),
        'pt-sans'             => array( 'title' => 'PT Sans',             'css' => '"PT Sans", sans-serif',             'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '700', '400italic', '700italic' ) ),
        'roboto-condensed'    => array( 'title' => 'Roboto Condensed',    'css' => '"Roboto Condensed", sans-serif',    'styles' => array( 'normal', 'italic' ), 'weights' => array( '300italic', '400italic', '700italic', '300', '400', '700' ) ),
        'source-sans-pro'     => array( 'title' => 'Source Sans Pro',     'css' => '"Source Sans Pro", sans-serif',     'styles' => array( 'normal', 'italic' ), 'weights' => array( '300', '400', '700', '300italic', '400italic', '700italic' ) ),
        'ubuntu'              => array( 'title' => 'Ubuntu',              'css' => '"Ubuntu", sans-serif',              'styles' => array( 'normal', 'italic' ), 'weights' => array( '300', '400', '700', '300italic', '400italic', '700italic' ) ),

        'lobster'             => array( 'title' => 'Lobster',             'css' => '"Lobster", cursive',                'styles' => array( 'normal' ),           'weights' => array( '400' )  ),
        'comfortaa'           => array( 'title' => 'Comfortaa',           'css' => '"Comfortaa", cursive',              'styles' => array( 'normal' ),           'weights' => array( '400', '300', '700' ) ),
        'chewy'               => array( 'title' => 'Chewy',               'css' => '"Chewy", cursive',                  'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'changa-one'          => array( 'title' => 'Changa One',          'css' => '"Changa One", cursive',             'styles' => array( 'normal', 'italic' ), 'weights' => array( '400', '400italic' ) ),
        'fredoka-one'         => array( 'title' => 'Fredoka One',         'css' => '"Fredoka One", cursive',            'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'special-elite'       => array( 'title' => 'Special Elite',       'css' => '"Special Elite", cursive',          'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'luckiest-guy'        => array( 'title' => 'Luckiest Guy',        'css' => '"Luckiest Guy", cursive',           'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'playball'            => array( 'title' => 'Playball',            'css' => '"Playball", cursive',               'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'squada-one'          => array( 'title' => 'Squada One',          'css' => '"Squada One", cursive',             'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'patua-one'           => array( 'title' => 'Patua One',           'css' => '"Patua One", cursive',              'styles' => array( 'normal' ),           'weights' => array( '400' ) ),

        'crafty-girls'        => array( 'title' => 'Crafty Girls',        'css' => '"Crafty Girls", cursive',           'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'shadows-into-light'  => array( 'title' => 'Shadows Into Light',  'css' => '"Shadows Into Light", cursive',     'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'tangerine'           => array( 'title' => 'Tangerine',           'css' => '"Tangerine", cursive',              'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'dancing-script'      => array( 'title' => 'Dancing Script',      'css' => '"Dancing Script", cursive',         'styles' => array( 'normal' ),           'weights' => array( '400', '700' ) ),
        'indie-flower'        => array( 'title' => 'Indie Flower',        'css' => '"Indie Flower", cursive',           'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'coming-soon'         => array( 'title' => 'Coming Soon',         'css' => '"Coming Soon", cursive',            'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'rock-salt'           => array( 'title' => 'Rock Salt',           'css' => '"Rock Salt", cursive',              'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
        'amatic-sc'           => array( 'title' => 'Amatic SC',           'css' => '"Amatic SC", cursive',              'styles' => array( 'normal' ),           'weights' => array( '400', '700' ) ),
        'handlee'             => array( 'title' => 'Handlee',             'css' => '"Handlee", cursive',                'styles' => array( 'normal' ),           'weights' => array( '400' ) ),
    );

    update_option( 'leviosa_google_fonts', serialize( $fonts ) );

}
endif; // leviosa_setup
add_action( 'after_setup_theme', 'leviosa_setup' );


/**
 * Register widgetized area and update sidebar with default widgets.
 */
function leviosa_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'leviosa' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

    register_sidebar( array(
        'name'          => __( 'Header Sidebar', 'leviosa' ),
        'id'            => 'header-sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Sidebar', 'leviosa' ),
        'id'            => 'footer-sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Blog Sidebar', 'leviosa' ),
        'id'            => 'blog-sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    $options  = get_option('option_tree');
    $sidebars = $options['custom_sidebars'];

    if ( ! empty($sidebars) ) {
        foreach( $sidebars as $sb ) {
            register_sidebar( array(
                'name'          => __( $sb['title'], THEME_SLUG ),
                'id'            => strtolower( str_replace( ' ', '-', trim( $sb['sidebar_name'] ) ) ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );
        }
    }
}
add_action( 'widgets_init', 'leviosa_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function leviosa_scripts() {
    /* =Default Font
    ------------------------ */
    wp_enqueue_style( 'leviosa-open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700|Open+Sans+Condensed:300,700' );

    /* =Grid
    ------------------------ */
    wp_register_style( 'leviosa-grid', get_template_directory_uri() . '/css/grid.css' );
    wp_enqueue_style( 'leviosa-grid' );

    /* =Font Awesome
    ------------------------ */
    wp_register_style( 'font-awesome', get_template_directory_uri() . '/assets/fontAwesome/style.css' );
    wp_enqueue_style( 'font-awesome' );

    /* =Main Style
    ------------------------ */
	wp_enqueue_style( 'leviosa-style', get_stylesheet_uri() );

    /* =Navigation script
    ------------------------ */
	wp_enqueue_script( 'leviosa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'leviosa-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

    /* =Comments
    ------------------------ */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    $loaded_fonts = array( 'open-sans', 'open-sans-condensed' );

    /* =Options General Font
    ------------------------ */

    /* =Menu Font
    ------------------------ */
    $menu_font_settings = ot_get_option( 'menu_font' );
    if ( isset($menu_font_settings['font-family']) && ! in_array( $menu_font_settings['font-family'], $loaded_fonts ) ) {
        $menu_font_url  = leviosa_get_font_url( $menu_font_settings['font-family'] );

        if ( false !== $menu_font_url ) wp_enqueue_style( 'leviosa-menu-font', $menu_font_url );
        array_push( $loaded_fonts, $menu_font_settings['font-family'] );
    }

    /* =Content Font
    ------------------------ */
    $content_font_settings = ot_get_option( 'content-font' );
    if ( isset($content_font_settings['font-family']) && ! in_array( $content_font_settings['font-family'], $loaded_fonts ) ) {
        $content_font_url = leviosa_get_font_url( $content_font_settings['font-family'] );

        if ( false !== $content_font_url ) wp_enqueue_style( 'leviosa-content-font', $content_font_url );
        array_push( $loaded_fonts, $content_font_settings['font-family'] );
    }

    /* =H1 Font
    ------------------------ */
    $h1_font_settings = ot_get_option( 'h1_font' );
    if ( isset($h1_font_settings['font-family']) && ! in_array( $h1_font_settings['font-family'], $loaded_fonts ) ) {
        $h1_font_url = leviosa_get_font_url( $h1_font_settings['font-family'] );

        if ( false !== $h1_font_url ) wp_enqueue_style( 'leviosa-h1-font', $h1_font_url );
        array_push( $loaded_fonts, $h1_font_settings['font-family'] );
    }

    /* =H2 Font
    ------------------------ */
    $h2_font_settings = ot_get_option( 'h2_font' );
    if ( isset($h2_font_settings['font-family']) && ! in_array( $h2_font_settings['font-family'], $loaded_fonts ) ) {
        $h2_font_url = leviosa_get_font_url( $h2_font_settings['font-family'] );

        if ( false !== $h2_font_url ) wp_enqueue_style( 'leviosa-h1-font', $h2_font_url );
        array_push( $loaded_fonts, $h2_font_settings['font-family'] );
    }

    /* =H3 Font
    ------------------------ */
    $h3_font_settings = ot_get_option( 'h3_font' );
    if ( isset($h3_font_settings['font-family']) && ! in_array( $h3_font_settings['font-family'], $loaded_fonts ) ) {
        $h3_font_url = leviosa_get_font_url( $h3_font_settings['font-family'] );

        if ( false !== $h3_font_url ) wp_enqueue_style( 'leviosa-h1-font', $h3_font_url );
        array_push( $loaded_fonts, $h3_font_settings['font-family'] );
    }

    /* =Footer Font
    ------------------------ */
    $footer_font_settings = ot_get_option( 'footer_content_font' );
    if ( isset($footer_font_settings['font-family']) && ! in_array( $footer_font_settings['font-family'], $loaded_fonts ) ) {
        $footer_font_url = leviosa_get_font_url( $footer_font_settings['font-family'] );

        if ( false !== $footer_font_url ) wp_enqueue_style( 'leviosa-footer-content-font', $footer_font_url );
        array_push( $loaded_fonts, $footer_font_settings['font-family'] );
    }

    /* =Footer Link Font
    ------------------------ */
    $footer_font_link_settings = ot_get_option( 'footer_link_font' );
    if ( isset($footer_font_settings['font-family']) && ! in_array( $footer_font_settings['font-family'], $loaded_fonts ) ) {
        $footer_font_link_url = leviosa_get_font_url( $footer_font_link_settings['font-family'] );

        if ( false !== $footer_font_link_url ) {
            wp_enqueue_style( 'leviosa-footer-link-font', $footer_font_link_url );
            array_push( $loaded_fonts, $footer_font_link_settings['font-family'] );
        }
    }

    /* =Footer Link Hover Font
    ------------------------ */
    $footer_font_link_hover_settings = ot_get_option( 'footer_link_hover_font' );
    if ( isset($footer_font_link_hover_settings['font-family']) && ! in_array( $footer_font_link_hover_settings['font-family'], $loaded_fonts ) ) {
        $footer_font_link_hover_url = leviosa_get_font_url( $footer_font_link_hover_settings['font-family'] );

        if ( false !== $footer_font_link_hover_url ) {
            wp_enqueue_style( 'leviosa-footer-link-font', $footer_font_link_hover_url );
            array_push( $loaded_fonts, $footer_font_link_hover_settings['font-family'] );
        }
    }

    /* =Footer Title Font
    ------------------------ */
    $footer_font_title_settings = ot_get_option( 'footer_title_font' );
    if ( isset($footer_font_title_settings['font-family']) && ! in_array( $footer_font_title_settings['font-family'], $loaded_fonts ) ) {
        $footer_font_title_url = leviosa_get_font_url( $footer_font_title_settings['font-family'] );

        if ( false !== $footer_font_title_url ) {
            wp_enqueue_style( 'leviosa-footer-title-font', $footer_font_title_url );
            array_push( $loaded_fonts, $footer_font_title_settings['font-family'] );
        }
    }

    /* =Sidebar Content Font
    ------------------------ */
    $sidebar_content_font_settings = ot_get_option( 'sidebar_content_font' );
    if ( isset($sidebar_content_font_settings['font-family']) && ! in_array( $sidebar_content_font_settings['font-family'], $loaded_fonts ) ) {
        $sidebar_content_font_url = leviosa_get_font_url( $sidebar_content_font_settings['font-family'] );

        if ( false !== $sidebar_content_font_url ) {
            wp_enqueue_style( 'leviosa-sidebar-content-font', $sidebar_content_font_url );
            array_push( $loaded_fonts, $sidebar_content_font_settings['font-family'] );
        }
    }

    /* =Sidebar Title Font
    ------------------------ */
    $sidebar_title_font_settings = ot_get_option( 'sidebar_title_font' );
    if ( isset($sidebar_title_font_settings['font-family']) && ! in_array( $sidebar_title_font_settings['font-family'], $loaded_fonts ) ) {
        $sidebar_title_font_url = leviosa_get_font_url( $sidebar_title_font_settings['font-family'] );

        if ( false !== $sidebar_title_font_url ) {
            wp_enqueue_style( 'leviosa-sidebar-title-font', $sidebar_title_font_url );
            array_push( $loaded_fonts, $sidebar_title_font_settings['font-family'] );
        }
    }

    /* =Theme specific adjustments for sliders as background
    -------------------------------------------- */
    if( is_home() || is_front_page() ) {
        switch( ot_get_option( 'general_slider_type', 'gs_leviosa_slider' ) ){
            case 'gs_leviosa_slider':
                wp_enqueue_style( 'lv-leviosa-slider-css', get_template_directory_uri() . '/css/leviosa-slider.css' );
                wp_enqueue_script( 'lv-leviosa-slider-js', get_template_directory_uri() . '/js/leviosa-slider.js', array('jquery') );
                break;
            case 'gs_layer_slider':
                wp_enqueue_style( 'lv-layer-slider-css', get_template_directory_uri() . '/css/layer-slider.css' );
                wp_enqueue_script( 'lv-layer-slider-css', get_template_directory_uri() . '/js/layer-slider.js', array('jquery'), null, true );
                break;
            case 'gs_widgetkit_slider':
                wp_enqueue_style( 'lv-widgetkit-css', get_template_directory_uri() . '/css/widgetkit-slider.css' );
                wp_enqueue_script( 'lv-widgetkit-js', get_template_directory_uri() . '/js/widgetkit-slider.js', array('jquery'), null, true );
        }
    }


    /* =Favicon
    --------------------------------------------- */
    $favicon = ot_get_option( 'favicon', false );
    if ( $favicon ) {
        echo '<link rel="shortcut icon" href="' . $favicon . '" />';
    }

    /* =Apple Touch
    --------------------------------------------- */
    $apple_touch = ot_get_option( 'ios_icon', false );
    if ( $apple_touch ) {
        echo '<link rel="apple-touch-icon-precomposed" href="' . $apple_touch . '"/>';
    }

    /* =Ajax HTML5
    --------------------------------------------- */
    if ( ot_get_option( 'content_loading', 'normal' ) == 'ajax' ) {
        wp_enqueue_style( 'lv-transition-css', get_template_directory_uri() . '/css/page-transitions.css' );
        wp_register_script( 'lv-ajaxhtml5-js', get_template_directory_uri() . '/js/ajaxhtml5.js', array('jquery'), null, true );
        wp_enqueue_script( 'lv-ajaxhtml5-js' );
        $ajax_settings = array(
            'animationSpeed'     => (int)ot_get_option( 'animation_speed', '300' ),
            'animationFrom'      => ot_get_option( 'input_animation', 'from_right' ),
            'animationTo'        => ot_get_option( 'output_animation', 'to_left' ),
            'gifAjaxLoader'      => ot_get_option( 'gif_ajax_loader', 'loading_3' ),
            'loadersUri'         => get_template_directory_uri() . '/img/ajax/',
            'homeUrl'            => home_url()
        );
        wp_localize_script( 'lv-ajaxhtml5-js', 'ajaxSettings', $ajax_settings );
    }

}
add_action( 'wp_enqueue_scripts', 'leviosa_scripts' );

/* =Custom Header Scripts
------------------------------- */
function leviosa_custom_header_script() {
    $in_footer = ot_get_option( 'show_js_in_footer', array() );
    $custom_code = ot_get_option( 'custom_js_code', false );
    $custom_css = ot_get_option( 'custom_code', false );

    if ( $custom_css )
        echo '<style type="text/css">' . $custom_css . '</style>';

    if ( empty( $in_footer ) && $custom_code )
        echo '<script type="text/javascript">' . $custom_code . '</script>';
}
add_action( 'wp_head', 'leviosa_custom_header_script' );


/* =Custom Footer Scripts
------------------------------- */
function leviosa_custom_footer_script() {
    $in_footer = ot_get_option( 'show_js_in_footer', array() );
    $custom_code = ot_get_option( 'custom_js_code', false );

    if ( ! empty( $in_footer ) && $custom_code )
        echo '<script type="text/javascript">' . $custom_code . '</script>';
}
add_action( 'wp_footer', 'leviosa_custom_footer_script' );

/* =Custom CSS
------------------------------- */
add_action( 'wp_head', 'leviosa_custom_css_setup' );
function leviosa_custom_css_setup() {
    echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/css/dinamic.css.php" />';
    echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/css/responsive.css" />';
}

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require THEME_PATH . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require THEME_PATH . '/inc/extras.php';

/**
 * Integrate Options Tree
 */
require THEME_PATH . '/theme-settings.php';

/**
 * Customizer additions.
 */
require THEME_PATH . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require THEME_PATH . '/inc/jetpack.php';

/**
 * Custom CSS
 */
//require THEME_PATH . '/inc/custom-css.php';

/**
 * Custom Post Types
 */
require THEME_PATH . '/inc/cpts.php';

/**
 * Shortcodes
 */
require THEME_PATH . '/inc/shortcodes.php';

/**
 * Integrated Widgets
 */
require THEME_PATH . '/inc/widgets.php';

/**
 * Custom MCE Buttons
 */
//require get_template_directory() . '/inc/mce.php';

/**
 * Format Hooks and Filters
 */
require THEME_PATH . '/inc/filterhooks.php';