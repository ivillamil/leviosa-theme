<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package leviosa
 */
?>

	</div><!-- #content -->

    <?php $footer = ot_get_option( 'footer' ); ?>
    <?php if ( ! 1 == $footer[0] ) : ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="footer-wrapper">
            <div class="site-info">
                <?php do_action( 'leviosa_credits' ); ?>
                <?php if ( $credits = ot_get_option( 'footer_credits', false ) ) : ?>
                    <?php echo do_shortcode( $credits ); ?>
                <?php else : ?>
                    <a href="http://wordpress.org/" rel="generator"><?php printf( __( 'Proudly powered by %s', 'leviosa' ), 'WordPress' ); ?></a>
                    <span class="sep"> | </span>
                    <?php printf( __( 'Theme: %1$s by %2$s.', 'leviosa' ), 'leviosa', '<a href="http://annoyingwizard.com/leviosa" rel="designer">Annoying Wizard</a>' ); ?>
                <?php endif; ?>
            </div><!-- .site-info -->

            <?php dynamic_sidebar( 'footer-sidebar' ) ?>
        </div><!-- .footer-wrapper -->

        <?php $slider_controls = ot_get_option( 'slider_controls', array() );  ?>
        <?php if ( isset( $slider_controls[0] ) && 'show' == $slider_controls[0] && ( is_home() || is_front_page() ) ) : ?>
            <div class="slider-controls">
                <?php
                $slider_controls_style = ot_get_option( 'slider_controls_style', 'rounded' );
                switch( $slider_controls_style ) {
                    case 'arrow':
                        echo '<a href="#prev" class="prev arrow"><i class="levicon-arrow-left"></i></a>';
                        echo '<a href="#next" class="next arrow"><i class="levicon-arrow-right"></i></a>';
                        break;
                    case 'circle_arrow':
                        echo '<a href="#prev" class="prev circle-arrow"><i class="levicon-circle-arrow-left"></i></a>';
                        echo '<a href="#next" class="next circle-arrow"><i class="levicon-circle-arrow-right"></i></a>';
                        break;
                    case 'circle_rounded':
                        echo '<a href="#prev" class="prev circle-rounded"><i class="levicon-chevron-sign-left"></i></a>';
                        echo '<a href="#next" class="next circle-rounded"><i class="levicon-chevron-sign-right"></i></a>';
                        break;
                    case 'double_thin':
                        echo '<a href="#prev" class="prev double-thin"><i class="levicon-double-angle-left"></i></a>';
                        echo '<a href="#next" class="next double-thin"><i class="levicon-double-angle-right"></i></a>';
                        break;
                    case 'play_controls':
                        echo '<a href="#prev" class="prev play-controls"><i class="levicon-backward"></i></a>';
                        echo '<a href="#next" class="next play-controls"><i class="levicon-forward"></i></a>';
                        break;
                    case 'rounded':
                        echo '<a href="#prev" class="prev rounded"><i class="levicon-chevron-left"></i></a>';
                        echo '<a href="#next" class="next rounded"><i class="levicon-chevron-right"></i></a>';
                        break;
                    case 'thin':
                        echo '<a href="#prev" class="prev thin"><i class="levicon-angle-left"></i></a>';
                        echo '<a href="#next" class="next thin"><i class="levicon-angle-right"></i></a>';
                        break;
                    case 'triangle':
                        echo '<a href="#prev" class="prev triangle"><i class="levicon-caret-left"></i></a>';
                        echo '<a href="#next" class="next triangle"><i class="levicon-caret-right"></i></a>';
                        break;
                }
                ?>
            </div><!-- .slider-cotnrols -->
        <?php endif; ?>

	</footer><!-- #colophon -->

    <?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>