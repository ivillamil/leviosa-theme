<?php

?>
<article id="post-<?php the_ID() ?>" <?php post_class( 'space-bottom-3 ' ) ?>>
    <div class="entry-media">
        <?php the_content() ?>
    </div>

    <div class="entry-header">
        <h1 class="entry-title">
            <span class="icon-wrapper"><i class="levicon-picture"></i></span>
            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h1>

        <?php if ( 'post' == get_post_type() ) : ?>
            <div class="entry-meta"><?php leviosa_posted_on(); ?></div>

            <div class="entry-meta">
                <?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
                    <?php
                    /* translators: used between list items, there is a space after the comma */
                    $categories_list = get_the_category_list( __( ', ', 'leviosa' ) );
                    if ( $categories_list && leviosa_categorized_blog() ) :
                        ?>
                        <span class="cat-links">
                    <?php printf( __( '| %1$s', 'leviosa' ), $categories_list ); ?>
                </span>
                    <?php endif; // End if categories ?>

                    <?php
                    /* translators: used between list items, there is a space after the comma */
                    $tags_list = get_the_tag_list( '', __( ', ', 'leviosa' ) );
                    if ( $tags_list ) :
                        ?>
                        <span class="tags-links">
                    <?php printf( __( '| Tagged %1$s', 'leviosa' ), $tags_list ); ?>
                </span>
                    <?php endif; // End if $tags_list ?>

                <?php endif; // End if 'post' == get_post_type() ?>

                <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
                    <span class="comments-link">| <?php comments_popup_link( __( 'Leave a comment', 'leviosa' ), __( '1 Comment', 'leviosa' ), __( '% Comments', 'leviosa' ) ); ?></span>
                <?php endif; ?>

                <?php edit_post_link( __( 'Edit', 'leviosa' ), '<span class="edit-link">', '</span>' ); ?>
            </div><!-- .entry-meta -->
        <?php endif; ?>
    </div>

    <?php if ( 'post' == get_post_type() ) : ?>
        <div class="entry-date-meta">
            <?php leviosa_posted_meta(); ?>
        </div><!-- .entry-meta -->
    <?php endif; ?>
</article>