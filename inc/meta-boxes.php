<?php

/**
 * Initialize the meta boxes.
 */
add_action( 'admin_init', 'custom_meta_boxes' );

function custom_meta_boxes() {

    $link_mb = array(
        'id'        => 'link_mb',
        'title'     => __( 'Link', THEME_SLUG ),
        'desc'      => '',
        'pages'     => array('post'),
        'context'   => 'normal',
        'priority'  => 'high',
        'fields'    => array(
            array(
                'id'    => 'lv_link_title',
                'label' => __( 'Link Title', THEME_SLUG ),
                'desc'  => '',
                'type'  => 'text'
            ),
            array(
                'id'    => 'lv_link_src',
                'label' => __( 'Link Src', THEME_SLUG ),
                'desc'  => '',
                'type'  => 'text'
            )
        )
    );

    ot_register_meta_box( $link_mb );

    $quote_mb = array(
        'id'        => 'quote_mb',
        'title'     => __( 'Quote', THEME_SLUG ),
        'desc'      => '',
        'pages'     => array('post'),
        'context'   => 'normal',
        'priority'  => 'high',
        'fields'    => array(
            array(
                'id'    => 'lv_quote_name',
                'label' => __( 'Quote Name', THEME_SLUG ),
                'desc'  => '',
                'type'  => 'text'
            ),
            array(
                'id'    => 'lv_quote_src',
                'label' => __( 'Quote Source', THEME_SLUG ),
                'desc'  => '',
                'type'  => 'text'
            )
        )
    );

    ot_register_meta_box( $quote_mb );

    $video_mb = array(
        'id'        => 'video_mb',
        'title'     => __( 'Video', THEME_SLUG ),
        'desc'      => '',
        'pages'     => array('post'),
        'context'   => 'normal',
        'priority'  => 'high',
        'fields'    => array(
            array(
                'id'    => 'lv_video_link',
                'label' => __( 'Video Link', THEME_SLUG ),
                'desc'  => __( 'Paste here the link to the video, it can be from Vimeo or Youtube.<br>http://youtu.be/vP2PDz9NMa4, http://vimeo.com/84867203' ),
                'type'  => 'text'
            ),
            array(
                'id'    => 'lv_embed_video',
                'label' => __( 'Embeded Video', THEME_SLUG ),
                'desc'  => __( 'Paste here the video embedable code, this option will override the option above', THEME_SLUG ),
                'type'  => 'textarea-simple',
                'rows'  => '2'
            ),
            array(
                'id'    => 'lv_video_proportions',
                'label' => __( 'Proportions', THEME_SLUG ),
                'desc'  => __( 'Mantain the aspect ratio.', THEME_SLUG ),
                'type'  => 'checkbox',
                'choices' => array(
                    array(
                        'value' => 'constrain',
                        'label' => __('Constrain Size', THEME_SLUG)
                    )
                )
            ),
            array(
                'id'    => 'lv_video_width',
                'label' => __( 'Width', THEME_SLUG ),
                'desc'  => __( 'Size in pixels.', THEME_SLUG ),
                'type'  => 'text',
                'std'   => 560,
                'class' => 'width-10'
            ),
            array(
                'id'    => 'lv_video_height',
                'label' => __( 'Height', THEME_SLUG ),
                'desc'  => __( 'Size in pixels.', THEME_SLUG ),
                'type'  => 'text',
                'std'   => 315,
                'class' => 'width-10'
            ),
            array(
                'id'    => 'lv_video_player',
                'label' => __( 'Player Settings', THEME_SLUG ),
                'desc'  => __( '', THEME_SLUG ),
                'type'  => 'checkbox',
                'std'   => '',
                'choices' => array(
                    array(
                        'value' => 'fullscreen',
                        'label' => __( 'Allow Fullscreen', THEME_SLUG )
                    ),
                    array(
                        'value' => 'controls',
                        'label' => __( 'Show Controls', THEME_SLUG )
                    )
                )
            )
        )
    );

    ot_register_meta_box( $video_mb );

    $my_meta_box = array(
        'id'        => 'bg_perpage',
        'title'     => __( 'Background', THEME_SLUG ),
        'desc'      => __( '', THEME_SLUG ),
        'pages'     => array( 'post', 'page', 'project', 'team', 'service' ),
        'context'   => 'normal',
        'priority'  => 'high',
        'fields'    => array(
            array(
                'id'          => 'pp_background',
                'label'       => __( ' ', THEME_SLUG ),
                'desc'        => '',
                'std'         => '',
                'type'        => 'background',
                'class'       => '',
                'choices'     => array()
            )
        )
    );

    ot_register_meta_box( $my_meta_box );

    $body_metabox = array(
        'id'       => 'body_perpage',
        'title'    => __( 'Body Settings', THEME_SLUG ),
        'desc'     => __( '' ),
        'pages'    => array( 'post', 'page', 'project', 'team', 'service' ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
            array(
                'id'      => 'pp_hide_body',
                'label'   => __( 'Hide Content', THEME_SLUG ),
                'desc'    => __( 'Hides the entire body content from page.', THEME_SLUG ),
                'std'     => '',
                'type'    => 'checkbox',
                'choices' => array(
                    array(
                        'label' => 'Hide',
                        'value' => 'hide'
                    )
                )
            ),
            array(
                'id'     => 'pp_body_position',
                'label'  => __( 'Body Position', THEME_SLUG ),
                'desc'   => '',
                'type'   => 'radio-image',
                'std'    => 'center'
            )
        )
    );

    if ( ot_get_option( 'content_loading', 'normal' ) == 'ajax' ) {
        $body_metabox['fields'][] = array(
            'id'     => 'pp_output_animation',
            'label'  => __( 'Output Animation', THEME_SLUG ),
            'desc'   => '',
            'std'    => 'to_left',
            'type'   => 'radio-image'
        );

        $body_metabox['fields'][] = array(
            'id'     => 'pp_input_animation',
            'label'  => __( 'Input Animation', THEME_SLUG ),
            'desc'   => '',
            'std'    => 'from_right',
            'type'   => 'radio-image'
        );
    }

    ot_register_meta_box( $body_metabox );

}