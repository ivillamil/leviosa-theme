<?php

class Leviosa_Shortcodes {

    function __construct() {
        add_shortcode( 'latest_posts', array( &$this, 'latest_posts' ) );
        add_shortcode( 'featured_post', array( &$this, 'featured_post' ) );
        add_shortcode( 'featured_page', array( &$this, 'featured_page' ) );
        add_shortcode( 'leviosa_video', array( &$this, 'video' ) );

        /* =Grid ============= */
        add_shortcode( 'row', array( &$this, 'row' ) );
        add_shortcode( 'one_column', array( &$this, 'one_column' ) );
        add_shortcode( 'two_columns', array( &$this, 'two_columns' ) );
        add_shortcode( 'three_columns', array( &$this, 'three_columns' ) );
        add_shortcode( 'four_columns', array( &$this, 'four_columns' ) );
        add_shortcode( 'five_columns', array( &$this, 'five_columns' ) );
        add_shortcode( 'six_columns', array( &$this, 'six_columns' ) );
        add_shortcode( 'seven_columns', array( &$this, 'seven_columns' ) );
        add_shortcode( 'eight_columns', array( &$this, 'eight_columns' ) );
        add_shortcode( 'nine_columns', array( &$this, 'nine_columns' ) );
        add_shortcode( 'ten_columns', array( &$this, 'ten_columns' ) );
        add_shortcode( 'eleven_columns', array( &$this, 'eleven_columns' ) );
        add_shortcode( 'twelve_columns', array( &$this, 'twelve_columns' ) );
    }

    public function latest_posts( $attrs, $content = null ) {
        extract( shortcode_atts( array(
            'type'      => 'post',
            'no_posts'  => 5,
            'offset'    => 0,
            'date'      => true,
            'excerpt'   => true,
            'size'      => 'thumbnail',
            'thumbnail' => true,
            'wrapper'   => 'ul' //ul|div
        ), $attrs ) );

        $loop = new WP_Query( array(
            'post_type'      => $type,
            'post_type'      => $type,
            'posts_per_page' => $no_posts,
            'orderby'        => 'post_date',
            'order'          => 'DESC',
            'offset'         => $offset
        ) );


        if ( ! $loop->have_posts() )
            return '<p class="error">'. __( 'No posts found', THEME_SLUG ) .'</p>';

        ob_start();
        echo ( $wrapper == 'ul' ) ? '<ul>' : '<div>';

        while( $loop->have_posts() ) {
            $loop->the_post();

            echo ( $wrapper == 'ul' ) ? '<li>' : '';
            include THEME_PATH . '/inc/shortcodes/posts.php';
            echo ( $wrapper == 'ul' ) ? '</li>' : '';
        }

        echo ( $wrapper == 'ul' ) ? '</ul>' : '</div>';
        wp_reset_postdata();

        return do_shortcode( ob_get_clean() );
    }

    public function featured_post( $attrs, $content = null ) {
        extract( shortcode_atts( array(
            'post_id'        => 0,
            'type'           => 'post',
            'title'          => 'true',
            'excerpt'        => 'false',
            'thumbnail'      => 'true',
            'thumbnail_pos'  => 'before', // before|after
            'thumbnail_size' => 'thumbnail'
        ), $attrs ) );

        $loop = new WP_Query( array(
            'post_type'      => $type,
            'orderby'        => 'post_date',
            'order'          => 'desc',
            'posts_per_page' => 1
        ) );

        if ( 0 !== $post_id )
            $loop->set( 'p', (int) $post_id );

        ob_start();
        while( $loop->have_posts() ) : $loop->the_post();
            include THEME_PATH . '/inc/shortcodes/featured_post.php';
        endwhile;

        wp_reset_postdata();

        return do_shortcode( ob_get_clean() );
    }

    public function featured_page( $attrs, $content = null ) {
        extract( shortcode_atts( array(
            'page_id'         => 0,
            'title'           => true,
            'content'         => true,
            'thumbnail'       => true,
            'thumbnail_pos'   => 'before',
            'thumbnail_size'  => 'thumbnail'
        ), $attrs ) );

        $loop = new WP_Query( array(
            'post_type'       => 'page',
            'orderby'         => 'post_date',
            'order'           => 'desc',
            'posts_per_page'  => 1
        ) );

        if ( 0 !== $page_id )
            $loop->set( 'p', (int) $post_id );

        ob_start();

        while( $loop->have_posts() ) : $loop->the_post();
            include THEME_PATH . '/inc/shortcodes/featured_page.php';
        endwhile;

        wp_reset_postdata();

        return do_shortcode( ob_get_clean() );
    }

    public function video( $attrs, $content = null ) {
        extract( shortcode_atts( array(
            'from'       => 'vimeo',
            'id'         => '',
            'width'      => '560',
            'height'     => '315',
            'fullscreen' => 'false',
            'controls'   => 'false'
        ), $attrs ) );

        $fullscreen = ( $fullscreen == 'false' ) ? 0 : 1;
        $controls   = ( $controls == 'false' ) ? 0 : 1;

        ob_start();

        include THEME_PATH . '/inc/shortcodes/video_' . $from . '.php';

        return ob_get_clean();
    }

    /**
     * Grid Functions
     */
    public function row( $attrs, $content = null ) {
        extract( shortcode_atts( array(
            'collapse' => false
        ), $attrs ) );

        $html = '<div class="row '. (($collapse)?'collapse':'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function one_column( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="one columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function two_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="two columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function three_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="three columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function four_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="four columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function five_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="five columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function six_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="six columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function seven_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="seven columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function eight_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="eight columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function nine_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="nine columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function ten_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="ten columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function eleven_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="eleven columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

    public function twelve_columns( $attrs, $content = null ) {
        extract( shortcode_atts( array( 'end' => false, 'snap' => false ), $attrs ) );
        $html = '<div class="twelve columns '.(($end)?'end':'') . (($snap)?' snap-' . $snap:'') .'">' . do_shortcode( $content ) . '</div>';
        return $html;
    }

}

new Leviosa_Shortcodes();