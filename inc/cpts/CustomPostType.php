<?php

class CustomPostType {

    protected $singular = '';
    protected $plural   = '';
    protected $columns  = array();

    /**
     * Determine if the current user has the relevant permissions
     *
     * @param $post_id
     * @return bool
     */
    protected function can_save_data( $post_id ) {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return;
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) )
                return;
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) )
                return;
        }

        return true;
    }

    /**
     * Register a Custom Post Type
     *
     * @param $single
     * @param $plural
     * @param null $supports
     */
    protected function register_post_type( $single, $plural, $options = array() ) {

        $labels = array(
            'name'			     => _x( ucfirst( $plural ), 'post type general name' ),
            'singular_name'	     => _x( ucfirst( "$single" ), 'post type singular name' ),
            'add_new'		     => _x( "Add New", "$single", THEME_SLUG ),
            'add_new_item'	     => __( "Add New ". ucfirst( $single ) ."", THEME_SLUG ),
            'edit_item'		     => __( "Edit ". ucfirst( $single ) ."", THEME_SLUG ),
            'new_item'		     => __( "New ". ucfirst( $single ) ."", THEME_SLUG ),
            'all_items'		     => __( "All ". ucfirst( $plural ) ."", THEME_SLUG ),
            'view_item'		     => __( "View ". ucfirst( $single ) ."", THEME_SLUG ),
            'search_items'	     => __( "Search ". ucfirst( $plural ) ."", THEME_SLUG ),
            'not_found'		     => __( "No ". ucfirst( $plural ) ." found", THEME_SLUG ),
            'not_found_in_trash' => __( "No ". ucfirst( $plural ) ." found in trash", THEME_SLUG ),
            'parent_item_colon'	 => '',
            'menu_name'		     => ucfirst( $plural )
        );

        $args = array(
            'labels'		     => $labels,
            'description'        => '',
            'public'		     => true,
            'publicly_queryable' => true,
            'show_ui'		     => true,
            'show_in_menu'	     => true,
            'query_var'		     => true,
            'rewrite'		     => true,
            'capability_type'    => 'post',
            'has_archive'	     => true,
            'hierarchical'	     => false,
            'menu_position'	     => null,
            'menu_icon'          => null,
            'supports'		     => array(
                                        'title',
                                        'editor',
                                        'author',
                                        'thumbnail',
                                        'excerpt',
                                        'trackbacks',
                                        'custom-fields',
                                        'comments',
                                        'revisions',
                                        'page-attributes' )
        );

        $args = array_merge( $args, $options );

        register_post_type( $single, $args );
    }

    /**
     * Register a new column in the admin custom type listing
     *
     * @param  $title
     * @param  $name
     * @return void
     */
    protected function register_columns() {
        add_action( 'manage_edit-' . $this->singular . '_columns', array( &$this, 'add_columns' ) );
        add_action( 'manage_posts_custom_column', array( &$this, 'columns_info' ) );
        add_filter( 'manage_edit-' . $this->singular . '_sortable_columns', array( &$this, 'sort_columns' ) );
        add_filter( 'request', array( &$this, 'order_by' ) );
    }

    function add_columns( $columns ) {
        foreach( $this->columns as $column ) {
            if ( isset( $column['pos'] ) && $column['pos'] == 'begin' )
                $columns = array_merge( array( 'cb' => '', $column['name'] => $column['title'] ), $columns );
            else
                $columns[ $column['name'] ] = $column['title'];
        }

        return $columns;
    }

    function columns_info( $column ) {
        foreach( $this->columns as $col ) {
            if ( $col['name'] == $column ) {
                $meta = get_post_meta( get_the_ID(), $col['name'], true );
                if ( $col['type'] == 'image' )
                    echo '<img src="'. get_thumb_from_url( $meta, 'thumbnail' ) .'" height=70>';
                else
                    echo $meta;
            }
        }
    }

    function sort_columns( $columns ) {
        foreach( $this->columns as $col ) {
                $columns[ $col['name'] ] = $col['title'];
        }

        return $columns;
    }

    function order_by( $vars ) {
        if ( ! is_admin() )
            return $vars;

        foreach( $this->columns as $col ) {
            if ( isset( $vars['orderby'] ) && $col['name'] == $vars['orderby'] )
                $vars = array_merge( $vars, array( 'meta_key' => $col['name'], 'orderby' => 'meta_value_num' ) );
        }

        return $vars;
    }

    /**
     * Register Taxonomy
     *
     * @param  $singular
     * @param  $plural
     * @return void
     */
    protected function register_taxonomy( $singular, $plural, $options = array() ) {
        $labels = array(
            'name'              => _x( ucwords( $plural ), 'taxonomy general name', THEME_SLUG ),
            'title'             => _x( ucwords( $singular ), 'taxonomy singular name', THEME_SLUG ),
            'add_new'           => __( 'Add New ' . ucwords( $singular ), THEME_SLUG ),
            'add_new_item'      => __( 'Add New ' . ucwords( $singular ), THEME_SLUG ),
            'new_item_name'     => __( 'New '. ucwords( $singular .' Name' ), THEME_SLUG ),
            'edit_item'         => __( 'Edit ' . ucwords( $singular ), THEME_SLUG ),
            'search_items'      => __( 'Search ' . ucwords( $plural ), THEME_SLUG ),
            'all_items'         => __( 'All ' . ucwords( $plural ), THEME_SLUG ),
            'parent_item'       => __( 'Parent ' . ucwords( $singular ), THEME_SLUG ),
            'parent_item_colon' => __( 'Parent ' . ucwords( $singular . ':' ), THEME_SLUG ),
            'update_item'       => __( 'Update ' . ucwords( $singular ), THEME_SLUG ),
            'menu_name'         => __( ucwords( $plural ), THEME_SLUG )
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'query_var'         => true,
            'rewrite'           => true
        );

        $args = array_merge( $args, $options );

        register_taxonomy( $singular, $this->singular, $args);
    }


    /**
     * Render a Template File
     *
     * @param $filePath
     * @param null $viewData
     * @return string
     */
    public function get_template_part( $filePath, $data = array() ) {
        ( ! empty( $data ) ) ? extract( $data ) : null;

        ob_start();
        include ("$filePath");
        $template = ob_get_contents();
        ob_end_clean();

        return $template;
    }
}