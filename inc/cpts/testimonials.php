<?php

class Testimonials extends CustomPostType {

    function __construct() {

        $this->singular = 'testimonial';
        $this->plural   = 'testimonials';
        $this->columns  = array(
            array( 'title' => __( 'Author', THEME_SLUG ), 'name' => 'lev_testimonial-author', 'pos' => 'end', 'type' => 'text' ),
            array( 'title' => __( 'Photo', THEME_SLUG ), 'name' => 'lev_testimonial-image', 'pos' => 'begin', 'type' => 'image' )
        );

        $this->register_post_type( $this->singular, $this->plural, array(
            'menu_position'  => apply_filters( 'leviosa_testimonial_menu-position', 5 ),
            'supports'       => apply_filters( 'leviosa_testimonial_supports', array( 'title', 'editor', 'custom-fields' ) ),
            'rewrite'        => apply_filters( 'leviosa_testimonial_rewrite', true )
        ) );

        $this->register_columns();

        add_action( 'admin_init', array( &$this, 'metabox_setup' ) );

    }

    function metabox_setup() {
        $testimonial_mb = array(
            'id'       => 'testimonial_metabox',
            'title'    => __( 'Details', THEME_SLUG ),
            'desc'     => apply_filters( 'leviosa_testimonial_mb-desc', '' ),
            'pages'    => array( "$this->singular" ),
            'context'  => apply_filters( 'leviosa_testimonial_mb-context', 'normal' ),
            'priority' => apply_filters( 'leviosa_testimonial_mb-priority', 'high' ),
            'fields'   => array(
                array(
                    'label'     => __( 'Author Name', THEME_SLUG ),
                    'id'        => 'lev_testimonial-author',
                    'type'      => 'text',
                    'desc'      => __( 'The author if this testimonial', THEME_SLUG ),
                    'std'       => '',
                    'rows'      => '',
                    'post_type' => '',
                    'taxonomy'  => '',
                    'class'     => ''
                ),
                array(
                    'label'     => __( 'Author Image', THEME_SLUG ),
                    'id'        => 'lev_testimonial-image',
                    'type'      => 'upload',
                    'desc'      => __( "Author's Picture. Preferred size 300px x 300px", THEME_SLUG ),
                    'std'       => '',
                    'rows'      => '',
                    'post_type' => '',
                    'taxonomy'  => '',
                    'class'     => ''
                ),
                array(
                    'label'     => __( 'Company', THEME_SLUG ),
                    'id'        => 'lev_testimonial-company',
                    'type'      => 'text',
                    'desc'      => ''
                )
            )
        );


        /**
         * Register our meta boxes using the
         * ot_register_meta_box() function.
         */
        ot_register_meta_box( $testimonial_mb );
    }
}