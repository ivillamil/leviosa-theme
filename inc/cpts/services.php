<?php

class Services extends CustomPostType {
    function __construct() {
        $this->singular = 'service';
        $this->plural   = 'services';
        $this->columns  = array(
            array( 'title' => __( 'Service Type', THEME_SLUG ), 'name' => 'service_type', 'pos' => 'end', 'type' => 'text' )
        );

        $this->register_post_type( $this->singular, $this->plural, array(
            'menu_position' => apply_filters( THEME_SLUG . '_service_menu-position', 5 ),
            'supports'      => apply_filters( THEME_SLUG . '_service_supports', array( 'title', 'editor', 'thumbnail', 'custom-fields' ) ),
            'rewrite'       => apply_filters( THEME_SLUG . '_service_rewite', true )
        ) );

        $this->register_columns();

        $this->register_taxonomy( 'service type', 'service types' );
    }
}