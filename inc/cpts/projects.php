<?php

class Projects extends CustomPostType {
    function __construct() {
        $this->singular = 'project';
        $this->plural   = 'projects';
        $this->columns  = array(
            array( 'title' => __( 'Category', THEME_SLUG ), 'name' => 'project_category', 'pos' => 'end', 'type' => 'text' )
        );

        $this->register_post_type( $this->singular, $this->plural, array(
            'menu_position' => apply_filters( THEME_SLUG . '_project_menu-position', 5 ),
            'supports'      => apply_filters( THEME_SLUG . '_project_supports', array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields' ) ),
            'rewrite'       => apply_filters( THEME_SLUG . '_project_rewrite', true )
        ) );

        $this->register_columns();

        $this->register_taxonomy( 'project category', 'project categories' );

        add_action( 'admin_init', array( &$this, 'metabox_setup' ) );
    }

    public function metabox_setup() {
        $project_mb = array(
            'id'       => 'project_metabox',
            'title'    => apply_filters( THEME_SLUG . '_project_mb_title', __( 'Details', THEME_SLUG ) ),
            'desc'     => apply_filters( THEME_SLUG . '_project_mb_desc', '' ),
            'pages'    => array( "$this->singular" ),
            'context'  => apply_filters( THEME_SLUG . '_project_mb_context', 'normal' ),
            'priority' => apply_filters( THEME_SLUG . '_project_mb_priority', 'high' ),
            'fields'   => array(
                array(
                    'label'    => __( 'URL', THEME_SLUG ),
                    'id'       => 'lev_project_url',
                    'type'     => 'text',
                    'desc'     => __( 'The url of the project if it is online', THEME_SLUG ),
                    'std'      => ''
                ),
                array(
                    'label'    => __( 'Gallery', THEME_SLUG ),
                    'id'       => 'lev_project_gallery',
                    'type'     => 'list-item',
                    'desc'     => __( 'Collection of images and/or videos', THEME_SLUG ),
                    'settings' => array(
                        array(
                            'label' => __( 'File', THEME_SLUG ),
                            'id'    => 'lev_project_gallery_file',
                            'type'  => 'upload',
                            'desc'  => '',
                            'std'   => ''
                        ),
                        array(
                            'label' => __( 'External File Url', THEME_SLUG ),
                            'id'    => 'lev_project_gallery_exernal_file',
                            'type'  => 'text',
                            'desc'  => __( 'This will override the previous option', THEME_SLUG ),
                            'std'   => ''
                        ),
                        array(
                            'label' => __( 'Url', THEME_SLUG ),
                            'id'    => 'lev_project_gallery_url',
                            'type'  => 'text',
                            'desc'  => ''
                        ),
                        array(
                            'label' => __( 'Description', THEME_SLUG ),
                            'id'    => 'lev_project_gallery_desc',
                            'type'  => 'textarea-simple',
                            'desc'  => '&nbsp;'
                        )
                    )
                )
            )
        );

        /**
         * Register our meta boxes using the
         * ot_register_meta_box() function.
         */
        ot_register_meta_box( $project_mb );
    }
}