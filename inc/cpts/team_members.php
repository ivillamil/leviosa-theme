<?php

class TeamMembers extends CustomPostType {
    function __construct() {
        $this->singular = 'team';
        $this->plural   = 'teams';
        $this->columns  = array();

        $this->register_post_type( $this->singular, $this->plural, array(
            'menu_position' => apply_filters( THEME_SLUG . '_team_menu-position', 5 ),
            'supports'      => apply_filters( THEME_SLUG . '_team_supports', array( 'title', 'editor', 'thumbnail', 'custom-fields' ) ),
            'rewrite'       => apply_filters( THEME_SLUG . '_team_rewrite', true )
        ) );

        $this->register_columns();

        $this->register_taxonomy( 'members category', 'members categories' );
    }
}