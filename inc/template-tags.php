<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package leviosa
 */

if ( ! function_exists( 'leviosa_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @return void
 */
function leviosa_paging_nav() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'leviosa' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'leviosa' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'leviosa' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'leviosa_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 *
 * @return void
 */
function leviosa_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'leviosa' ); ?></h1>
		<div class="nav-links">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'leviosa' ) ); ?>
			<?php next_post_link(     '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link',     'leviosa' ) ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'leviosa_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function leviosa_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php _e( 'Pingback:', 'leviosa' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'leviosa' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

	<?php else : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
		<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
			<footer class="comment-meta">
				<div class="comment-author vcard">
					<?php if ( 0 != $args['avatar_size'] ) { echo get_avatar( $comment, $args['avatar_size'] ); } ?>
					<?php printf( __( '%s <span class="says">says:</span>', 'leviosa' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author -->

				<div class="comment-metadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
						<time datetime="<?php comment_time( 'c' ); ?>">
							<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'leviosa' ), get_comment_date(), get_comment_time() ); ?>
						</time>
					</a>
					<?php edit_comment_link( __( 'Edit', 'leviosa' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .comment-metadata -->

				<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'leviosa' ); ?></p>
				<?php endif; ?>
			</footer><!-- .comment-meta -->

			<div class="comment-content">
				<?php comment_text(); ?>
			</div><!-- .comment-content -->

			<?php
				comment_reply_link( array_merge( $args, array(
					'add_below' => 'div-comment',
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
					'before'    => '<div class="reply">',
					'after'     => '</div>',
				) ) );
			?>
		</article><!-- .comment-body -->

	<?php
	endif;
}
endif; // ends check for leviosa_comment()

if ( ! function_exists( 'leviosa_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function leviosa_posted_on() {
	$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string .= '<time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	printf( __( '<span class="byline">Posted by %1$s</span>', 'leviosa' ),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s">%2$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_html( get_the_author() )
		)
	);
}
endif;


if ( ! function_exists( 'leviosa_posted_meta' ) ) :

function leviosa_posted_meta() {
    $time_string = '<span class="entry-month">%1$s</span><span class="entry-day">%2$s</span>';

    $time_string = sprintf( $time_string,
        esc_html( date('M', get_the_date( 'U' )) ),
        esc_html( date('d', get_the_date( 'U' )) )
    );

    echo $time_string;
}

endif;

/**
 * Returns true if a blog has more than 1 category.
 */
function leviosa_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so leviosa_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so leviosa_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in leviosa_categorized_blog.
 */
function leviosa_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'leviosa_category_transient_flusher' );
add_action( 'save_post',     'leviosa_category_transient_flusher' );


/**
 * Returns a thumbnail object from an image url
 */
function get_thumb_from_url( $attachment_url = null, $size = 'thumbnail' ) {
    if ( ! isset(  $attachment_url) )
        return false;

    global $wpdb;
    $upload_path   = wp_upload_dir();
    $attachment_id = false;
    $sql           = '';


    if ( false !== strpos( $attachment_url, $upload_path['baseurl'] ) ) {
        // If this is the URL of an auto-generated thumbnail, get the URL of the original image
        $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

        // Remove the upload path base directory from the attachment URL
        $attachment_url = str_replace( $upload_path['baseurl'] . '/', '', $attachment_url );

        // Finally, run a custom database query to get the attachment ID from the modified attachment URL
        $sql = "SELECT wposts.ID
                FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
                WHERE wposts.ID = wpostmeta.post_id
                    AND wpostmeta.meta_key = '_wp_attached_file'
                    AND wpostmeta.meta_value = '%s'
                    AND wposts.post_type = 'attachment'";

        $attachment_id = $wpdb->get_var( $wpdb->prepare( $sql, $attachment_url ) );
    }

    $new_attachment_url = wp_get_attachment_image_src( $attachment_id, $size );
    return $new_attachment_url[0];
}

/**
 * Google API url for the selected font-family
 */
function leviosa_get_font_url( $font_name ) {
    $font_options  = unserialize( get_option( 'leviosa_google_fonts' ) );
    $base_font_url = 'http://fonts.googleapis.com/css?family=';

    if ( ! isset( $font_options[$font_name] ) )
        return false;

    $font_url = $base_font_url . str_replace( ' ', '+', $font_options[$font_name]['title'] );

    //var_dump( get_option( 'leviosa_google_fonts' ) ); die();

    if ( count( $font_options[$font_name]['weights'] ) > 1 )
        $font_url .= ':' . implode( ',', $font_options[$font_name]['weights'] );

    return $font_url;
}

/**
 * CSS Property declaration for the selected font-family
 */
function leviosa_get_font_css( $font_name ) {
    $font_options = unserialize( get_option( 'leviosa_google_fonts' ) );
    $font_css     = $font_options[$font_name]['css'];

    return $font_css;
}

/**
 * Returns the rgba color for the color and alpha given
 */
function leviosa_get_rgba( $hex, $alpha ) {
    list( $r, $g, $b ) = sscanf( $hex, '#%02x%02x%02x' );
    return "rgba($r,$g,$b,$alpha)";
}

/**
 * Returns a template for static background
 */
function leviosa_get_static_background( $settings ) {

    $cover = true;
    if( isset($settings['background-repeat']) && !empty($settings['background-repeat']) && $settings['background-repeat'] !== 'no-repeat' )
        $cover = false;

    echo '<div class="static-slider"
                  style="background-color: ' . $settings['background-color'] . ';
                         background-repeat: ' . $settings['background-repeat'] . ';
                         background-attachment: ' . $settings['background-attachment'] . ';
                         background-position: ' . $settings['background-position'] . ';
                         background-image: url(' . $settings['background-image'] . ');
                         '. ( (!$cover) ? 'background-size:auto' : '' ) . '"></div>';
}

/**
 * Return a template for leviosa slider
 */
function leviosa_get_slider() {
    $html = '';

    return $html;
}

/**
 * Get the video parts from links
 */
function leviosa_get_video_src( $link ) {

    $vimeo_patt   = '/vimeo{1}\.com{1}\/(\d{8})/'; // the id comes in array index 1
    $youtube_patt = '/youtu{1}\w*\.{1}\w*\/(watch?\?v=?)?(\w+)/'; // the id comes in array index 2

    preg_match( $vimeo_patt, $link, $matches );
    if ( ! empty( $matches ) ) {
        return array(
            'from' => 'vimeo',
            'id'   => $matches[1]
        );
    }

    preg_match( $youtube_patt, $link, $matches );
    if ( ! empty( $matches ) ) {
        return array(
            'from' => 'youtube',
            'id'   => $matches[2]
        );
    }

    return array();
}

/**
 * Get the map html from code
 */
function leviosa_get_map_fromcode( $code ) {
    $code = preg_replace( '/width="\d+"/', 'width="100%"', $code );
    $code = preg_replace( '/height="\d+"/', 'height="100%" style="width:100%;height:100%"', $code );
    return '<div class="map-wrapper">' . $code . '</div>';
}