<div class="hentry">
    <?php if ( $thumbnail && has_post_thumbnail() ) : ?>
    <div class="entry-image"><?php the_post_thumbnail( $size ) ?></div>
    <?php endif; ?>
    <div class="entry-header">
        <h2 class="entry-title"><?php the_title(); ?></h2>
        <?php if ( $date ) : ?>
        <div class="entry-meta"><?php leviosa_posted_on(); ?></div>
        <?php endif ?>
    </div>
    <div class="entry-content">
        <?php ( $excerpt ) ? the_excerpt() : the_content(); ?>
    </div>
</div>