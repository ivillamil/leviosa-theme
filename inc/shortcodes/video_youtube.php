<iframe
    width="<?php echo $width ?>"
    height="<?php echo $height ?>"
    src="//www.youtube.com/embed/<?php echo $id ?>?controls=<?php echo $controls ?>"
    frameborder="0"
    <?php if($fullscreen) : ?>
    webkitallowfullscreen
    mozallowfullscreen
    allowfullscreen
    <?php endif ?>></iframe>