<div class="hentry featured-page page-<?php echo get_the_ID() ?>">

    <?php  if ( 'true' == $thumbnail && $thumbnail_pos == 'before' && has_post_thumbnail( get_the_ID() ) ) : ?>
        <div class="image-wrapper post-thumbnail">
            <?php the_post_thumbnail( $thumbail_size ); ?>
        </div>
    <?php endif; ?>

    <?php do_action( THEME_SLUG . '_before_header' ); ?>
    <?php if ( 'true' == $title ) : ?>
        <div class="entry-header">
            <h2 class="entry-title"><?php the_title() ?></h2>
            <div class="entry-meta"><?php leviosa_posted_on(); ?></div>
        </div>
    <?php endif; ?>

    <?php if ( 'true' == $thumbnail && $thumbnail_pos == 'after' && has_post_thumbnail( get_the_ID() ) ) : ?>
        <div class="image-wrapper post-thumbnail">
            <?php the_post_thumbnail( $thumbnail_size ); ?>
        </div>
    <?php endif; ?>

    <?php do_action( THEME_SLUG . '_before_content' ); ?>
    <div class="entry-content">
        <?php if ( 'true' == $excerpt ) { the_excerpt(); } else { the_content(); } ?>
    </div>
    <?php do_action( THEME_SLUG . '_after_content' ); ?>

</div>