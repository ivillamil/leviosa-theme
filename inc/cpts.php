<?php

define( 'CPT_PATH', get_template_directory() . '/inc/cpts' );

require CPT_PATH . '/CustomPostType.php';

/**
 * PROJECTS
 */
require CPT_PATH . '/projects.php';

/**
 * SERVICES
 */
require CPT_PATH . '/services.php';

/**
 * TEAM MEMBERS
 */
require CPT_PATH . '/team_members.php';

/**
 * TESTIMONIALS
 */
require CPT_PATH . '/testimonials.php';

function leviosa_init_cpts() {
    $custom_types = ot_get_option( 'custom_types', array() );

    if ( empty( $custom_types ) )
        return false;

    if ( in_array( 'testimonial', $custom_types ) )
        new Testimonials();

    if ( in_array( 'services', $custom_types ) )
        new Services();

    if ( in_array( 'staff', $custom_types ) )
        new TeamMembers();

    if ( in_array( 'projects', $custom_types ) )
        new Projects();
}
add_action( 'init', 'leviosa_init_cpts' );

function leviosa_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'leviosa_rewrite_flush' );