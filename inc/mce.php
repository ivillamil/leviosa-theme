<?php

require '/mce/posts-mce-button.php';

add_action( 'admin_init', 'leviosa_mce_buttons' );

function leviosa_mce_buttons() {
    if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) )
    {
        new Leviosa_MCE_Button();
    }
}