<?php

class Leviosa_MCE_Button {

    public function __construct() {
        add_filter( 'mce_buttons', array( &$this, 'register_buttons' ) );
        add_filter( 'mce_external_plugins', array( &$this, 'external_plugins' ) );
    }

    public function external_plugins( $plugin_array ) {
        $plugin_array[THEME_SLUG . '_buttons_script'] = THEME_URI . '/inc/mce/js/leviosa-buttons.js';
        return $plugin_array;
    }

    public function register_buttons( $buttons ) {
        array_push( $buttons, THEME_SLUG . '_buttons' );
        return $buttons;
    }

}