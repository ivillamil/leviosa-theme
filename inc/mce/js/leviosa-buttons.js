;(function() {
    /* Register the buttons */
    tinymce.create('tinymce.plugins.LevPostsBtn', {
        init: function(ed, url) {
            /**
             * Inserts shortcode contents
             */
            ed.addButton( 'button_posts', {
                title: 'Posts Shortcodes',
                image: '',
                onclick: function() {
                    ed.selection.setContent('[latest_posts]');
                }
            } );
        }
    });
})();