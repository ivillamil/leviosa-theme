<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => 'You can create as many settings as your project requires and use them how you see fit. When you add a setting here, it will be available on the Theme Options page for use in your theme. To separate your settings into sections, click the "Add Section" button, fill in the input fields, and a new navigation menu item will be created.

All of the settings can be sorted and rearranged to your liking with Drag & Drop. Don\'t worry about the order in which you create your settings, you can always reorder them.'
    ),
    'sections'        => array( 
      array(
        'id'          => 'general',
        'title'       => '<i class="levicon-cogs ot-tab-icon"></i> General'
      ),
      array(
        'id'          => 'branding',
        'title'       => '<i class="levicon-design ot-tab-icon"></i> Branding'
      ),
      array(
        'id'          => 'header',
        'title'       => '<i class="levicon-header ot-tab-icon"></i> Header'
      ),
      array(
        'id'          => 'body',
        'title'       => '<i class="levicon-body ot-tab-icon"></i> Body'
      ),
      array(
        'id'          => 'footer',
        'title'       => '<i class="levicon-footer ot-tab-icon"></i> Footer'
      ),
      array(
        'id'          => 'sidebar_area',
        'title'       => '<i class="levicon-sidebar ot-tab-icon"></i> Sidebar Area'
      ),
      array(
        'id'          => 'home',
        'title'       => '<i class="levicon-home ot-tab-icon"></i> Home'
      ),
      array(
        'id'          => 'blog',
        'title'       => '<i class="levicon-comments ot-tab-icon"></i> Blog'
      ),
      array(
        'id'          => 'contact',
        'title'       => '<i class="levicon-envelope-alt ot-tab-icon"></i> Contact'
      ),
      array(
        'id'          => 'woo_commerce_settings',
        'title'       => '<i class="levicon-shopping-cart ot-tab-icon"></i> WooCommerce'
      ),
      array(
        'id'          => 'custom_types_settings',
        'title'       => '<i class="levicon-wordpress ot-tab-icon"></i> Custom Types'
      ),
      array(
        'id'          => 'translation',
        'title'       => '<i class="levicon-globe ot-tab-icon"></i> Translation'
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'content_loading',
        'label'       => 'Content Loading',
        'desc'        => '',
        'std'         => 'normal',
        'type'        => 'radio',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'ajax',
            'label'       => 'Ajax HTML5',
            'src'         => ''
          ),
          array(
            'value'       => 'normal',
            'label'       => 'Normal',
            'src'         => ''
          )
        ),
      ),
        array(
            'id'          => 'animation_speed',
            'label'       => __( 'Animation Speed', THEME_SLUG ),
            'desc'        => __( 'Time in milliseconds.', THEME_SLUG ),
            'std'         => '300',
            'type'        => 'numeric-slider',
            'section'     => 'general',
            'min_max_step'=> '100,3000,100'
        ),
        array(
            'id'          => 'input_animation',
            'label'       => __( 'Input Animation', THEME_SLUG ),
            'desc'        => __( 'Indicates the position from where the page will come.', THEME_SLUG ),
            'std'         => 'from_right',
            'type'        => 'radio-image',
            'section'     => 'general'
        ),
        array(
            'id'          => 'output_animation',
            'label'       => __( 'Output Animation', THEME_SLUG ),
            'desc'        => __( 'Indicates the direction where the page will leave the screen.', THEME_SLUG ),
            'std'         => 'to_left',
            'type'        => 'radio-image',
            'section'     => 'general'
        ),
        array(
            'id'          => 'gif_ajax_loader',
            'label'       => __( 'Ajax Loader', THEME_SLUG ),
            'desc'        => '',
            'std'         => 'loading_1',
            'type'        => 'radio-image',
            'section'     => 'general',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
        ),
        array(
            'id'          => 'toggle_content',
            'label'       => __( 'Toggle Content Button', THEME_SLUG ),
            'desc'        => __( 'Shows a button in the top right corner that allows the user to hide the content.', THEME_SLUG ),
            'std'         => '',
            'type'        => 'checkbox',
            'section'     => 'general',
            'choices'     => array(
                array(
                    'value'  => 'show',
                    'label'  => __( 'Show', THEME_SLUG ),
                    'src'    => ''
                )
            )
        ),
      array(
        'id'          => 'footer',
        'label'       => 'Footer',
        'desc'        => 'Activate this option to hide the site footer and all its widgets and sidebars',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => '1',
            'label'       => 'Hide footer',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'google_analytics',
        'label'       => 'Google Analytics Code',
        'desc'        => 'Paste here the code provided by google analytics in order to keep tracking the visits of the site.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'general',
        'rows'        => '4',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'custom_code',
        'label'       => 'Custom CSS Code',
        'desc'        => 'This area allows you to define custom css properties to modify some areas of the theme.',
        'std'         => '',
        'type'        => 'css',
        'section'     => 'general',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'custom_js_code',
        'label'       => 'Custom Javascript Code',
        'desc'        => 'Paste here the javascript code without the "" tags.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'general',
        'rows'        => '8',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'show_js_in_footer',
        'label'       => 'Javascript in Footer',
        'desc'        => 'By default, the code will be placed just before the closing "head" tag, if you activate this option the code will be placed at the bottom of the body just right before the "body" closing tag.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => '1',
            'label'       => 'yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'logo',
        'label'       => 'Logo',
        'desc'        => 'The file uploaded will be shown in the branding of the site including the login page.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_position',
        'label'       => 'Logo Position',
        'desc'        => 'Fine tune adjustments for the branding.',
        'std'         => '',
        'type'        => 'textblock-titled',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_size',
        'label'       => 'Logo Size',
        'desc'        => '',
        'std'         => '80',
        'type'        => 'numeric-slider',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '0,100,1',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_top',
        'label'       => 'Top',
        'desc'        => '',
        'std'         => '10',
        'type'        => 'numeric-slider',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '0,80,1',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_left',
        'label'       => 'Left',
        'desc'        => '',
        'std'         => '10',
        'type'        => 'numeric-slider',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '0,200,1',
        'class'       => ''
      ),
      array(
        'id'          => 'login_logo',
        'label'       => 'Login Logo',
        'desc'        => 'Leave this field empty if you want to use the same logo from the option above.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'login_background',
        'label'       => 'Login Background',
        'desc'        => '',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'favicon',
        'label'       => 'Favicon',
        'desc'        => 'Normally a .ico file or png with 32x32 pixels.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'ios_icon',
        'label'       => 'iOS Icon',
        'desc'        => '128x128 pixels',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'primary_color',
        'label'       => 'Primary Color',
        'desc'        => '',
        'std'         => '#4E067B',
        'type'        => 'colorpicker',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'secondary_color',
        'label'       => 'Secondary Color',
        'desc'        => '',
        'std'         => '#4fbd82',
        'type'        => 'colorpicker',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'tertiary_color',
        'label'       => 'Tertiary Color',
        'desc'        => '',
        'std'         => '#eeeeee',
        'type'        => 'colorpicker',
        'section'     => 'branding',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'header_color',
        'label'       => 'Header Background Color',
        'desc'        => 'Change the default color for the site\'s header.',
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'header_bg_opacity',
        'label'       => 'Header Background Color Opacity',
        'desc'        => '',
        'std'         => '80',
        'type'        => 'numeric-slider',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '0,100,1',
        'class'       => ''
      ),
      array(
        'id'          => 'header_font_color',
        'label'       => 'General Header Font Color',
        'desc'        => '',
        'std'         => '#ffffff',
        'type'        => 'colorpicker',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'menu_position',
        'label'       => 'Menu Position',
        'desc'        => '',
        'std'         => 'left',
        'type'        => 'select',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'left',
            'label'       => 'Left',
            'src'         => ''
          ),
          array(
            'value'       => 'right',
            'label'       => 'Right',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'mobile_menu_style',
        'label'       => 'Mobile Menu Style',
        'desc'        => '',
        'std'         => 'dropdown',
        'type'        => 'select',
        'section'     => 'header',
        'choices'     => array(
          array(
            'value'   => 'dropdown',
            'label'   => __( 'Drop Down', THEME_SLUG )
          ),
          array(
            'value'   => 'offcanvas',
            'label'   => __( 'Off Canvas', THEME_SLUG )
          )
        )
      ),
      array(
        'id'          => 'menu_font',
        'label'       => 'Menu Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'menu_font_hover',
        'label'       => 'Menu Font Hover',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'menu_hover_bg',
        'label'       => 'Menu Hover Background',
        'desc'        => '',
        'std'         => '#efefef',
        'type'        => 'colorpicker',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'menu_hover_use_primary',
        'label'       => 'User Primary Color',
        'desc'        => 'Enable this option to use the "Primary Color" from the <b>Branding</b> section instead of the color from option above.',
        'std'         => '0',
        'type'        => 'checkbox',
        'section'     => 'header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => '1',
            'label'       => 'Use primary color',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'body_size',
        'label'       => 'Body Size',
        'desc'        => '',
        'std'         => '960',
        'type'        => 'numeric-slider',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '600,1200,1',
        'class'       => ''
      ),
      array(
        'id'          => 'body_position',
        'label'       => 'Body Position',
        'desc'        => 'Select the position of the page\'s body, this layout will affect every section of the site at least that you specify another one in per page options.',
        'std'         => 'center',
        'type'        => 'radio-image',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'body_color',
        'label'       => 'Body Color',
        'desc'        => 'Change the background color of the body content.',
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'body_opacity',
        'label'       => 'Body Opacity',
        'desc'        => 'Define the opacity percent of the content body.',
        'std'         => '90',
        'type'        => 'numeric-slider',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '0,100,1',
        'class'       => ''
      ),
      array(
        'id'          => 'content_font',
        'label'       => 'Content Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'h1_font',
        'label'       => 'H1 Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'h2_font',
        'label'       => 'H2 Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'h3_font',
        'label'       => 'H3 Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'body',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'footer_credits',
        'label'       => 'Credits',
        'desc'        => 'Replace the default credits phrase in the footer.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'footer',
        'rows'        => '2',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'footer_color',
        'label'       => 'Footer Background Color',
        'desc'        => 'Change the default theme color for the footer.',
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'footer_bg_opacity',
        'label'       => 'Footer Background Color Opacity',
        'desc'        => '',
        'std'         => '90',
        'type'        => 'numeric-slider',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '0,100,1',
        'class'       => ''
      ),
      array(
        'id'          => 'menu_content_font',
        'label'       => 'Content Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'footer_title_font',
        'label'       => 'Title Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'footer_link_font',
        'label'       => 'Link Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'footer_link_hover_font',
        'label'       => 'Link Hover Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'general_layout',
        'label'       => 'Sidebar Position',
        'desc'        => 'Select the position of the sidebars.',
        'std'         => '',
        'type'        => 'radio-image',
        'section'     => 'sidebar_area',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'sidebar_bg',
        'label'       => 'Sidebar Background',
        'desc'        => '',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'sidebar_area',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'sidebar_content_font',
        'label'       => 'Content Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'sidebar_area',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'sidebar_title_font',
        'label'       => 'Title Font',
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'sidebar_area',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'custom_sidebars',
        'label'       => 'Custom Sidebars',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'sidebar_area',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'settings'    => array( 
          array(
            'id'          => 'sidebar_name',
            'label'       => 'Sidebar Name',
            'desc'        => 'This field is required',
            'std'         => '',
            'type'        => 'text',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => ''
          )
        )
      ),
        array(
            'id'          => 'general_slider_type',
            'label'       => 'Home Background',
            'desc'        => 'Sets the global background slider, but it can be override in each post/page background settings.',
            'std'         => 'gs_leviosa_slider',
            'type'        => 'select',
            'section'     => 'home',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
            'choices'     => array(
                array(
                    'value'       => 'gs_static_background',
                    'label'       => __( 'Static Background', THEME_SLUG ),
                    'src'         => ''
                ),
                array(
                    'value'       => 'gs_leviosa_slider',
                    'label'       => 'Leviosa Slider',
                    'src'         => ''
                ),
                array(
                    'value'       => 'gs_layer_slider',
                    'label'       => 'Layer Slider',
                    'src'         => ''
                ),
                array(
                    'value'       => 'gs_widgetkit_slider',
                    'label'       => 'Widgetkit',
                    'src'         => ''
                )
            ),
        ),
        array(
            'id'          => 'leviosa_static_bg',
            'label'       => __( 'Static Background', THEME_SLUG ),
            'desc'        => '',
            'std'         => '',
            'type'        => 'background',
            'section'     => 'home'
        ),
        array(
            'id'          => 'leviosa_slider_items',
            'label'       => 'Leviosa Slider',
            'desc'        => '',
            'std'         => '',
            'type'        => 'slider',
            'section'     => 'home',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => ''
        ),
        array(
            'id'          => 'layer_slider_id',
            'label'       => 'Layer Slider',
            'desc'        => '',
            'std'         => '',
            'type'        => 'layerslider-select',
            'section'     => 'home',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => ''
        ),
        array(
            'id'          => 'widgetkit_slider_id',
            'label'       => 'Widgetkit',
            'desc'        => '',
            'std'         => '',
            'type'        => 'widgetkit-select',
            'section'     => 'home',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => ''
        ),
        array(
            'id'          => 'slider_controls',
            'label'       => __( 'Slider Controls', THEME_SLUG ),
            'desc'        => __( 'In order to show this controls, the footer should be visible', THEME_SLUG ),
            'std'         => '',
            'type'        => 'checkbox',
            'section'     => 'home',
            'choices'     => array(
                array(
                    'value'       => 'show',
                    'label'       => 'Show controls',
                    'src'         => ''
                )
            )
        ),
        array(
            'id'          => 'slider_controls_style',
            'label'       => __( 'Slider Controls Style', THEME_SLUG ),
            'desc'        => '',
            'std'         => 'rounded',
            'type'        => 'radio-image',
            'section'     => 'home'
        ),
      array(
        'id'          => 'blog_style',
        'label'       => 'Blog Style',
        'desc'        => '',
        'std'         => 'blog-standard',
        'type'        => 'select',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'sphere',
            'label'       => 'Sphere',
            'src'         => ''
          ),
          array(
            'value'       => 'masonry',
            'label'       => 'Masonry',
            'src'         => ''
          ),
          array(
            'value'       => 'blog-standard',
            'label'       => 'Classic',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'blog_bg',
        'label'       => __( 'Blog Background' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'blog'
      ),
      array(
        'id'          => 'show_author',
        'label'       => 'Author',
        'desc'        => '',
        'std'         => '1',
        'type'        => 'checkbox',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => '1',
            'label'       => 'Show',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'show_date',
        'label'       => 'Date',
        'desc'        => '',
        'std'         => '1',
        'type'        => 'checkbox',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => '1',
            'label'       => 'Show',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'contact_template_page',
        'label'       => 'Contact Template Page',
        'desc'        => 'The contact template will be shown in this page',
        'std'         => '',
        'type'        => 'page-select',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'contact_template_layout',
        'label'       => 'Layout',
        'desc'        => '',
        'std'         => 'two',
        'type'        => 'radio-image',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'receiver_mail',
        'label'       => 'Receiver Mail',
        'desc'        => 'The mail form the contact form will be sent to this mail address.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'contact_form_title',
        'label'       => 'Contact Form Title',
        'desc'        => 'Leave empty to remove the title and it\'s space.',
        'std'         => 'Contact Us',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'contact_form_fields',
        'label'       => 'Contact Form Fields',
        'desc'        => 'Add the fields you want to include in the contact form.',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'settings'    => array( 
          array(
            'id'          => 'name',
            'label'       => 'Name',
            'desc'        => 'Only lowercase and underscore allowed.',
            'std'         => '',
            'type'        => 'text',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => ''
          ),
          array(
            'id'          => 'type',
            'label'       => 'Type',
            'desc'        => '',
            'std'         => '',
            'type'        => 'select',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
            'choices'     => array( 
              array(
                'value'       => 'checkbox',
                'label'       => 'Checkbox',
                'src'         => ''
              ),
              array(
                'value'       => 'radio',
                'label'       => 'Radio',
                'src'         => ''
              ),
              array(
                'value'       => 'select',
                'label'       => 'Select',
                'src'         => ''
              ),
              array(
                'value'       => 'text',
                'label'       => 'Text',
                'src'         => ''
              ),
              array(
                'value'       => 'textarea',
                'label'       => 'Textarea',
                'src'         => ''
              )
            ),
          ),
          array(
            'id'          => 'options',
            'label'       => 'Options',
            'desc'        => 'Coma separated items.
This will only affect the following types: Checkbox, Radio, Select',
            'std'         => '',
            'type'        => 'textarea-simple',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => ''
          ),
          array(
            'id'          => 'placeholder',
            'label'       => 'Placeholder',
            'desc'        => 'Help text inside of field.',
            'std'         => '',
            'type'        => 'text',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => ''
          ),
          array(
            'id'          => 'required',
            'label'       => 'Required',
            'desc'        => '',
            'std'         => 'no',
            'type'        => 'radio',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
            'choices'     => array( 
              array(
                'value'       => 'yes',
                'label'       => 'Yes',
                'src'         => ''
              ),
              array(
                'value'       => 'no',
                'label'       => 'No',
                'src'         => ''
              )
            ),
          )
        )
      ),
      array(
        'id'          => 'location',
        'label'       => 'Location',
        'desc'        => 'Indicate your location in the map.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'contact',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => 'map-selector'
      ),
      array(
        'id'          => 'map_settings',
        'label'       => 'Map',
        'desc'        => 'Choose the location where you want the map to be displayed. <br><br>
Alternatively you can choose "none" and include the map through the content editor button.',
        'std'         => 'background',
        'type'        => 'radio',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'below_content',
            'label'       => 'Below the content',
            'src'         => ''
          ),
          array(
            'value'       => 'below_form',
            'label'       => 'Below the form',
            'src'         => ''
          ),
          array(
            'value'       => 'background',
            'label'       => 'Background',
            'src'         => ''
          ),
          array(
            'value'       => 'none',
            'label'       => 'None',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'contact_email',
        'label'       => 'Contact Email Address',
        'desc'        => 'Email address shown on the page.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'email_icon',
        'label'       => 'Email Address Icon',
        'desc'        => 'By default <strong>Leviosa Theme</strong> will show this icon <i class="levicon-envelope-alt"></i> as the email icon, but you can replace with your own icon through this field.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'contact_phone',
        'label'       => 'Contact Phone',
        'desc'        => '<p> </p>',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'contact_phone_icon',
        'label'       => 'Contact Phone Icon',
        'desc'        => 'By default <strong>Leviosa Theme</strong> shows this icon <i class="levicon-phone" style="font-size:1.3em"></i> as the <strong>phone</strong> icon, but you can replace it with your own icon through this field.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'contact_address',
        'label'       => 'Contact Address',
        'desc'        => '<p> </p>',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'contact',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'contact_address_icon',
        'label'       => 'Contact Address Icon',
        'desc'        => 'By default <strong>Leviosa Theme</strong> shows this icon <i class="levicon-home" style="font-size:1.3em"></i> as the <strong>Address</strong> Icon, but you can replace it with your own icon through this field.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'enable_woo',
        'label'       => 'Enable WooCommerce',
        'desc'        => 'Enable the integration with WooCommerce plugin, this will activate additional options like the custom cart widget.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'woo_commerce_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => '1',
            'label'       => 'Enable',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'shop_layout',
        'label'       => 'Shop Layout',
        'desc'        => 'This option will affect how the shop pages displays the products when WooCommerce integration is enabled.',
        'std'         => 'four',
        'type'        => 'radio-image',
        'section'     => 'woo_commerce_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'cart_icon',
        'label'       => 'Cart Icon',
        'desc'        => 'By default <strong>Leviosa</strong> will show this icon <i class="levicon-shopping-cart" style="font-size:1.3em"></i> as the Cart icon, but you can replace it by your own icon through this field.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'woo_commerce_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'cpt_text',
        'label'       => 'Custom posts',
        'desc'        => '<div style="background:#efefef;padding:2em;border:1px solid #ccc"><i class="icon-exclamation-sign" style="font-size:2em;float:left;margin-right:.5em"></i> The Leviosa Theme comes with a bunch of custom post types already installed but they will only be active if you select so with the options below.</div>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'custom_types_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      ),
      array(
        'id'          => 'custom_types',
        'label'       => 'Custom Types',
        'desc'        => 'Select the custom types that will be shown in the dashboard.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'custom_types_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'testimonial',
            'label'       => 'Testimonials',
            'src'         => ''
          ),
          array(
            'value'       => 'projects',
            'label'       => 'Projects',
            'src'         => ''
          ),
          array(
            'value'       => 'services',
            'label'       => 'Services',
            'src'         => ''
          ),
          array(
            'value'       => 'staff',
            'label'       => 'Team Members',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'add_to_cart',
        'label'       => 'Add to cart',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'translation',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => ''
      )
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}