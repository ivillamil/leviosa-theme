<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package leviosa
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>

<?php $options = get_option('option_tree'); ?>
<body <?php body_class(); ?>>
<?php echo ot_get_option( 'google_analytics', '' ); ?>
<?php get_sidebar(); ?>

<?php
$body_visibility  = get_post_meta( get_the_ID(), 'pp_hide_body',          true );
$body_position    = get_post_meta( get_the_ID(), 'pp_body_position',      true );
$input_animation  = get_post_meta( get_the_ID(), 'pp_input_animation',    true );
$output_animation = get_post_meta( get_the_ID(), 'pp_output_animation',   true );

$settings = array(
    'body_visibility'   => $body_visibility[0],
    'body_position'     => $body_position,
    'input_animation'   => $input_animation,
    'output_animation'  => $output_animation
);

?>
<div id="page" class="hfeed site" data-settings="<?php echo esc_attr( json_encode( $settings ) ) ?>">

    <!-- Slider Background -->
    <div id="bg_slider" class="slider">
        <?php
        if( !is_home() && (is_front_page() || get_the_ID() == get_option('page_on_front')) ) :
            switch( ot_get_option( 'general_slider_type', 'gs_static_background' ) ) {
                case 'gs_static_background':
                    $bg_settings = ot_get_option( 'leviosa_static_bg' );
                    if ( !empty( $bg_settings ) )
                        leviosa_get_static_background( $bg_settings );
                    break;
                case 'gs_leviosa_slider':
                    $slider_settings = ot_get_option( 'leviosa_slider_items' );
                    if ( !empty( $slider_settings ) )
                        leviosa_get_slider();
                    break;
                case 'gs_layer_slider':
                    $layer_slider = ot_get_option( 'layer_slider_id', false );
                    if ( $layer_slider )
                        echo do_shortcode( '[layerslider id="' . $layer_slider . '"]' );
                    break;
                case 'gs_widgetkit_slider':
                    $widgetkit_slider = ot_get_option( 'widgetkit_slider_id', false );
                    if ( $widgetkit_slider )
                        echo do_shortcode( '[widgetkit id=' . $widgetkit_slider . ']' );
                    break;
            }
        elseif( get_the_ID() == ot_get_option( 'contact_template_page', 0 ) && $map_code = ot_get_option( 'location', false ) ) :
            echo leviosa_get_map_fromcode( $map_code );
        elseif( is_home() && ('none' !== ot_get_option( 'blog_bg', 'none' )) ) :
            $bg_settings = ot_get_option( 'blog_bg' );
            leviosa_get_static_background( $bg_settings );
        else :
            $bg_settings = get_post_meta( get_the_ID(), 'pp_background', true );
            if ( !empty( $bg_settings ) )
                leviosa_get_static_background( $bg_settings );
        endif;
        ?>
    </div><!-- #bg_slider.slider -->
    <!-- /Slider Background -->

	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header" role="banner">
        <div class="site-branding">
            <?php $logo = ot_get_option( 'logo', false ); ?>
            <h1 class="site-title <?php if( $logo ) { echo 'logo'; } ?>">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <?php if( $logo ) : ?>
                    <img src="<?php echo $logo; ?>" alt="<?php bloginfo( 'name' ) ?> logo">
                    <?php else : ?>
                    <?php bloginfo( 'name' ); ?>
                    <?php endif; ?>
                </a>
            </h1>
        </div>

        <a href="#sidebar" class="sidebar-toggle" id="sidebar-toggle"><i class="levicon-reorder"></i></a>

        <nav id="site-navigation" class="main-navigation <?php echo ot_get_option( 'menu_position' ); ?> <?php echo ot_get_option( 'mobile_menu_style' ); ?>" role="navigation">
            <h1 class="menu-toggle"><i class="levicon-reorder"></i><span><?php _e( 'Menu', 'leviosa' ); ?></span></h1>
            <!--<a class="skip-link screen-reader-text" href="#content">Skip to content</a>-->

            <?php wp_nav_menu( array(
                'theme_location' => 'primary',
                'link_before'    => '<span>',
                'link:after'     => '</span>') ); ?>
        </nav><!-- #site-navigation -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
