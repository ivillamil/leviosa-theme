<?php
$absolute_path = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once( $wp_load );


header("Content-type: text/css; charset: UTF-8");

$animation_speed      = ot_get_option( 'animation_speed', '300' );
$body_size            = ot_get_option( 'body_size', '960' );
$body_color           = ot_get_option( 'body_color', '#ffffff' );
$body_font_ot         = ot_get_option( 'content_font' );
$body_font            = array(
    'font-color'      => ( ! empty( $body_font_ot['font-color'] ) )      ? $body_font_ot['font-color']            : '#444444',
    'font-family'     => ( ! empty( $body_font_ot['font-family'] ) )     ? leviosa_get_font_css( $body_font_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $body_font_ot['font-size'] ) )       ? $body_font_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $body_font_ot['font-style'] ) )      ? $body_font_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $body_font_ot['font-weight'] ) )     ? $body_font_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $body_font_ot['text-decoration'] ) ) ? $body_font_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $body_font_ot['text-transform'] ) )  ? $body_font_ot['text-transform']  : 'none',
);
$body_opacity         = (int) ot_get_option( 'body_opacity', '100' ) / 100;
$body_color_rgba      = leviosa_get_rgba( $body_color, $body_opacity );
$footer_color         = ot_get_option( 'footer_color', '#ffffff' );
$footer_color_opacity = (int) ot_get_option( 'footer_bg_opacity', '90' ) / 100;
$footer_color_rgba    = leviosa_get_rgba( $footer_color, $footer_color_opacity );
$ft_content_font_ot   = ot_get_option( 'footer_content_font' );
$ft_content_font      = array(
    'font-color'      => ( ! empty( $ft_content_font_ot['font-color'] ) )      ? $ft_content_font_ot['font-color']      : '#444444',
    'font-family'     => ( ! empty( $ft_content_font_ot['font-family'] ) )     ? leviosa_get_font_css( $ft_content_font_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $ft_content_font_ot['font-size'] ) )       ? $ft_content_font_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $ft_content_font_ot['font-style'] ) )      ? $ft_content_font_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $ft_content_font_ot['font-weight'] ) )     ? $ft_content_font_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $ft_content_font_ot['text-decoration'] ) ) ? $ft_content_font_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $ft_content_font_ot['text-transform'] ) )  ? $ft_content_font_ot['text-transform']  : 'none',
);
$ft_link_font_ot      = ot_get_option( 'footer_link_font' );
$ft_link_font         = array(
    'font-color'      => ( ! empty( $ft_link_font_ot['font-color'] ) )      ? $ft_link_font_ot['font-color']      : '#444444',
    'font-family'     => ( ! empty( $ft_link_font_ot['font-family'] ) )     ? leviosa_get_font_css( $ft_link_font_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $ft_link_font_ot['font-size'] ) )       ? $ft_link_font_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $ft_link_font_ot['font-style'] ) )      ? $ft_link_font_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $ft_link_font_ot['font-weight'] ) )     ? $ft_link_font_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $ft_link_font_ot['text-decoration'] ) ) ? $ft_link_font_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $ft_link_font_ot['text-transform'] ) )  ? $ft_link_font_ot['text-transform']  : 'none',
);
$ft_title_font_ot     = ot_get_option( 'footer_title_font' );
$ft_title_font        = array(
    'font-color'      => ( ! empty( $ft_title_font_ot['font-color'] ) )      ? $ft_title_font_ot['font-color']      : '#444444',
    'font-family'     => ( ! empty( $ft_title_font_ot['font-family'] ) )     ? leviosa_get_font_css( $ft_title_font_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $ft_title_font_ot['font-size'] ) )       ? $ft_title_font_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $ft_title_font_ot['font-style'] ) )      ? $ft_title_font_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $ft_title_font_ot['font-weight'] ) )     ? $ft_title_font_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $ft_title_font_ot['text-decoration'] ) ) ? $ft_title_font_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $ft_title_font_ot['text-transform'] ) )  ? $ft_title_font_ot['text-transform']  : 'none',
);
$h1_body_font_ot      = ot_get_option( 'h1_font' );
$h1_body_font         = array(
    'font-color'      => ( ! empty( $h1_body_font_ot['font-color'] ) )      ? $h1_body_font_ot['font-color']      : '#444444',
    'font-family'     => ( ! empty( $h1_body_font_ot['font-family'] ) )     ? leviosa_get_font_css( $h1_body_font_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $h1_body_font_ot['font-size'] ) )       ? $h1_body_font_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $h1_body_font_ot['font-style'] ) )      ? $h1_body_font_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $h1_body_font_ot['font-weight'] ) )     ? $h1_body_font_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $h1_body_font_ot['text-decoration'] ) ) ? $h1_body_font_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $h1_body_font_ot['text-transform'] ) )  ? $h1_body_font_ot['text-transform']  : 'none',
);
$h2_body_font_ot      = ot_get_option( 'h2_font' );
$h2_body_font         = array(
    'font-color'      => ( ! empty( $h2_body_font['font-color'] ) )      ? $h2_body_font['font-color']      : '#444444',
    'font-family'     => ( ! empty( $h2_body_font['font-family'] ) )     ? leviosa_get_font_css( $h2_body_font['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $h2_body_font['font-size'] ) )       ? $h2_body_font['font-size']       : '1em',
    'font-style'      => ( ! empty( $h2_body_font['font-style'] ) )      ? $h2_body_font['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $h2_body_font['font-weight'] ) )     ? $h2_body_font['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $h2_body_font['text-decoration'] ) ) ? $h2_body_font['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $h2_body_font['text-transform'] ) )  ? $h2_body_font['text-transform']  : 'none',
);
$h3_body_font_ot      = ot_get_option( 'h3_font' );
$h3_body_font         = array(
    'font-color'      => ( ! empty( $h3_body_font['font-color'] ) )      ? $h3_body_font['font-color']      : '#444444',
    'font-family'     => ( ! empty( $h3_body_font['font-family'] ) )     ? leviosa_get_font_css( $h3_body_font['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $h3_body_font['font-size'] ) )       ? $h3_body_font['font-size']       : '1em',
    'font-style'      => ( ! empty( $h3_body_font['font-style'] ) )      ? $h3_body_font['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $h3_body_font['font-weight'] ) )     ? $h3_body_font['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $h3_body_font['text-decoration'] ) ) ? $h3_body_font['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $h3_body_font['text-transform'] ) )  ? $h3_body_font['text-transform']  : 'none',
);
$main_color_primary   = ot_get_option( 'primary_color' );
$main_color_secondary = ot_get_option( 'secondary_color' );
$main_color_tertiary  = ot_get_option( 'tertiary_color' );
$header_color         = ot_get_option( 'header_color', '#000000' );
$header_bg_opacity    = (int) ot_get_option( 'header_bg_opacity', '80' ) / 100;
$header_color_rgba    = leviosa_get_rgba( $header_color, $header_bg_opacity );
$header_font_color    = ot_get_option( 'header_font_color', '#ffffff' );
$logo_left            = ot_get_option( 'logo_left', 0 );
$logo_size            = ot_get_option( 'logo_size', 80 );
$logo_top             = ot_get_option( 'logo_top', 0 );
$menu_hover_primary   = (int) ot_get_option( 'menu_hover_use_primary', 0 );
$menu_hover_bg        = ot_get_option( 'menu_hover_bg', '#4E067B' );
$menu_font_ot         = ot_get_option( 'menu_font' );
$menu_font_hover_ot   = ot_get_option( 'menu_font_hover' );
$menu_font            = array(
    'font-color'      => ( ! empty( $menu_font_ot['font-color'] ) )      ? $menu_font_ot['font-color']      : '#444444',
    'font-family'     => ( ! empty( $menu_font_ot['font-family'] ) )     ? leviosa_get_font_css( $menu_font_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $menu_font_ot['font-size'] ) )       ? $menu_font_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $menu_font_ot['font-style'] ) )      ? $menu_font_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $menu_font_ot['font-weight'] ) )     ? $menu_font_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $menu_font_ot['text-decoration'] ) ) ? $menu_font_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $menu_font_ot['text-transform'] ) )  ? $menu_font_ot['text-transform']  : 'none'
);
$menu_font_hover      = array(
    'font-color'      => ( ! empty( $menu_font_hover_ot['font-color'] ) )      ? $menu_font_hover_ot['font-color']      : '#444444',
    'font-family'     => ( ! empty( $menu_font_hover_ot['font-family'] ) )     ? leviosa_get_font_css( $menu_font_hover_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $menu_font_hover_ot['font-size'] ) )       ? $menu_font_hover_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $menu_font_hover_ot['font-style'] ) )      ? $menu_font_hover_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $menu_font_hover_ot['font-weight'] ) )     ? $menu_font_hover_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $menu_font_hover_ot['text-decoration'] ) ) ? $menu_font_hover_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $menu_font_hover_ot['text-transform'] ) )  ? $menu_font_hover_ot['text-transform']  : 'none'
);
$sidebar_bg           = ot_get_option( 'sidebar_bg', '#444444' );
$sidebar_font_ot      = ot_get_option( 'sidebar_content_font', false );
$sidebar_content_font = array(
    'font-color'      => ( ! empty( $sidebar_font_ot['font-color'] ) )      ? $sidebar_font_ot['font-color']      : '#ffffff',
    'font-family'     => ( ! empty( $sidebar_font_ot['font-family'] ) )     ? leviosa_get_font_css( $sidebar_font_ot['font-family'] ) : 'sans-serif',
    'font-size'       => ( ! empty( $sidebar_font_ot['font-size'] ) )       ? $sidebar_font_ot['font-size']       : '1em',
    'font-style'      => ( ! empty( $sidebar_font_ot['font-style'] ) )      ? $sidebar_font_ot['font-style']      : 'normal',
    'font-weight'     => ( ! empty( $sidebar_font_ot['font-weight'] ) )     ? $sidebar_font_ot['font-weight']     : 'normal',
    'text-decoration' => ( ! empty( $sidebar_font_ot['text-decoration'] ) ) ? $sidebar_font_ot['text-decoration'] : 'none',
    'text-transform'  => ( ! empty( $sidebar_font_ot['text-transform'] ) )  ? $sidebar_font_ot['text-transform']  : 'none'
);
$sidebar_title_font_ot= ot_get_option( 'sidebar_title_font', false );
$sidebar_title_font   = array();


ob_start();

?>

        .main-navigation a {
            color:           <?php echo ( 1 == $menu_hover_primary ) ? $main_color_primary : $menu_font['font-color']; ?> !important;
            font-family:     <?php echo $menu_font['font-family']; ?> !important;
            font-size:       <?php echo $menu_font['font-size']; ?> !important;
            font-style:      <?php echo $menu_font['font-style']; ?> !important;
            font-weight:     <?php echo $menu_font['font-weight']; ?> !important;
            text-decoration: <?php echo $menu_font['text-decoration']; ?> !important;
            text-transform:  <?php echo $menu_font['text-transform']; ?> !important;
        }
        .main-navigation a:hover,
        .main-navigation .current-menu-item a {
            background:      <?php echo ( 1 == $menu_hover_primary ) ? $main_color_primary : $menu_hover_bg; ?> !important;
            color:           <?php echo $menu_font_hover['font-color']; ?> !important;
            font-family:     <?php echo $menu_font_hover['font-family']; ?> !important;
            font-size:       <?php echo $menu_font_hover['font-size']; ?> !important;
            font-style:      <?php echo $menu_font_hover['font-style']; ?> !important;
            font-weight:     <?php echo $menu_font_hover['font-weight']; ?> !important;
            text-decoration: <?php echo $menu_font_hover['text-decoration']; ?> !important;
            text-transform:  <?php echo $menu_font_hover['text-transform']; ?> !important;

        }
        .main-navigation ul ul {
            background-color: <?php echo $header_color_rgba; ?>;
        }
        #primary.from_bottom,
        #primary.from_left,
        #primary.from_right,
        #primary.from_top,
        #primary.to_bottom,
        #primary.to_left,
        #primary.to_right,
        #primary.to_top {
            -webkit-animation-duration: <?php echo $animation_speed ?>ms;
            -moz-animation-duration:    <?php echo $animation_speed ?>ms;
            -ms-animation-duration:     <?php echo $animation_speed ?>ms;
            animation-duration:         <?php echo $animation_speed ?>ms;
        }
        .sidebar-toggle,
        .menu-toggle {
            color: <?php echo ( 1 == $menu_hover_primary ) ? $main_color_primary : $menu_font['font-color']; ?> !important;
        }
        .sidebar-toggle:hover,
        .sidebar-toggle:active,
        .sidebar-toggle.active,
        .menu-toggle:hover,
        .menu-toggle:focus,
        .menu-toggle:active {
            background:      <?php echo ( 1 == $menu_hover_primary ) ? $main_color_primary : $menu_hover_bg; ?> !important;
            color:           <?php echo $menu_font_hover['font-color']; ?> !important;
        }
        .site-footer {
            background:      <?php echo $footer_color_rgba ?>;
            color:           <?php echo $ft_content_font['font-color']; ?>;
            font-family:     <?php echo $ft_content_font['font-family']; ?>;
            font-size:       <?php echo $ft_content_font['font-size']; ?>;
            font-style:      <?php echo $ft_content_font['font-style']; ?>;
            font-weight:     <?php echo $ft_content_font['font-weight']; ?>;
            text-decoration: <?php echo $ft_content_font['text-decoration']; ?>;
            text-transform:  <?php echo $ft_content_font['text-transform']; ?>;
        }
        .site-footer a {
            color:           <?php echo ($ft_link_font['font-color']) ? $ft_link_font['font-color'] : $main_color_primary; ?>;
            font-family:     <?php echo $ft_link_font['font-family']; ?>;
            font-size:       <?php echo $ft_link_font['font-size']; ?>;
            font-style:      <?php echo $ft_link_font['font-style']; ?>;
            font-weight:     <?php echo $ft_link_font['font-weight']; ?>;
            text-decoration: <?php echo $ft_link_font['text-decoration']; ?>;
            text-transform:  <?php echo $ft_link_font['text-transform']; ?>;
        }
        .site-footer .slider-controls a {
            color:           <?php echo $ft_link_font['font-color']; ?>;
        }
        .site-footer h2,
        .site-footer h3 {
            color:           <?php echo ($ft_title_font['font-color']) ? $ft_title_font['font-color'] : $main_color_primary; ?>;
            font-family:     <?php echo $ft_title_font['font-family']; ?>;
            font-size:       <?php echo $ft_title_font['font-size']; ?>;
            font-style:      <?php echo $ft_title_font['font-style']; ?>;
            font-weight:     <?php echo $ft_title_font['font-weight']; ?>;
            text-decoration: <?php echo $ft_title_font['text-decoration']; ?>;
            text-transform:  <?php echo $ft_title_font['text-transform']; ?>;
        }
        .site-header {
            background-color: <?php echo $header_color_rgba; ?>;
            color: <?php echo $header_font_color; ?>;
        }
        .site-header a,
        .site-header h1,
        .site-header h2,
        .site-header h3,
        .site-header .site-title,
        .site-header .site-description {
            color: <?php echo $header_font_color; ?>
        }
        .site-main {
            background-color: <?php echo $body_color_rgba; ?> !important;
            max-width: <?php echo $body_size . 'px'; ?>;
        }
        .blog-page .site-main {
            background: transparent !important;
        }
        .site-title.logo {
            margin-left: <?php echo $logo_left; ?>px;
            margin-top: <?php echo $logo_top; ?>px;
        }
        .site-title.logo img {
            width: <?php echo $logo_size; ?>% !important;
        }
        #toggle-button:hover,
        #toggle-button:active,
        #toggle-button:focus {
            background: <?php echo ( 1 == $menu_hover_primary ) ? $main_color_primary : $menu_hover_bg; ?> !important;
            color: <?php echo $menu_font_hover['font-color'] ?>;
        }

        /* =Body Styles
        --------------------------- */
        body,
        .entry-content,
        .entry-meta {
            color:           <?php echo $body_font['font-color']; ?>;
            font-family:     <?php echo $body_font['font-family']; ?> !important;
            font-size:       <?php echo $body_font['font-size']; ?>;
            font-style:      <?php echo $body_font['font-style']; ?> !mportant;
            font-weight:     <?php echo $body_font['font-weight']; ?> !important;
            text-decoration: <?php echo $body_font['text-decoration']; ?> !important;
            text-transform:  <?php echo $body_font['text-transform']; ?> !important;
        }
        .entry-meta {
            opacity: .9;
            font-size: .8em;
        }
        h1,
        h1.entry-title,
        h1.entry-title a {
            color:           <?php echo ($h1_body_font['font-color']) ? $h1_body_font['font-color'] : $main_color_primary; ?>;
            font-family:     <?php echo $h1_body_font['font-family']; ?>;
            font-size:       <?php echo $h1_body_font['font-size']; ?>;
            font-style:      <?php echo $h1_body_font['font-style']; ?>;
            font-weight:     <?php echo $h1_body_font['font-weight']; ?>;
            text-decoration: <?php echo $h1_body_font['text-decoration']; ?>;
            text-transform:  <?php echo $h1_body_font['text-transform']; ?>;
        }
        h1.entry-title a {
            color: <?php echo $main_color_primary; ?>
        }
        h2,
        h2.entry-title,
        h2.entry-title a {
            color:           <?php echo ($h2_body_font['font-color']) ? $h2_body_font['font-color'] : $main_color_secondary; ?>;
            font-family:     <?php echo $h2_body_font['font-family']; ?>;
            font-size:       <?php echo $h2_body_font['font-size']; ?>;
            font-style:      <?php echo $h2_body_font['font-style']; ?>;
            font-weight:     <?php echo $h2_body_font['font-weight']; ?>;
            text-decoration: <?php echo $h2_body_font['text-decoration']; ?>;
            text-transform:  <?php echo $h2_body_font['text-transform']; ?>;
        }
        h3,
        h3.entry-title,
        h3.entry-title a {
            color:           <?php echo ($h2_body_font['font-color']) ? $h2_body_font['font-color'] : $main_color_secondary; ?>;
            font-family:     <?php echo $h2_body_font['font-family']; ?>;
            font-size:       <?php echo $h2_body_font['font-size']; ?>;
            font-style:      <?php echo $h2_body_font['font-style']; ?>;
            font-weight:     <?php echo $h2_body_font['font-weight']; ?>;
            text-decoration: <?php echo $h2_body_font['text-decoration']; ?>;
            text-transform:  <?php echo $h2_body_font['text-transform']; ?>;
        }

        /* =Widget Area
        --------------------------- */
        .widget-area {
            background-color:      <?php echo ( isset( $sidebar_bg['background-color'] ) && !empty( $sidebar_bg['background-color'] ) )           ? $sidebar_bg['background-color']      : '#444444' ?>;
            background-repeat:     <?php echo ( isset( $sidebar_bg['background-repeat'] ) && !empty( $sidebar_bg['background-repeat'] ) )         ? $sidebar_bg['background-repeat']     : 'no-repeat' ?>;
            background-attachment: <?php echo ( isset( $sidebar_bg['background-attachment'] ) && !empty( $sidebar_bg['background-attachment'] ) ) ? $sidebar_bg['background-attachment'] : 'normal' ?>;
            backgorund-position:   <?php echo ( isset( $sidebar_bg['background-position'] ) && !empty( $sidebar_bg['background-position'] ) )     ? $sidebar_bg['background-position']   : 'center center' ?>;
            background-image:      <?php echo ( isset( $sidebar_bg['background-image'] ) && !empty( $sidebar_bg['background-image'] ) )           ? "url(" . $sidebar_bg['background-image'] .")" : 'none' ?>;

            color:           <?php echo $sidebar_content_font['font-color'] ?>;
            font-family:     <?php echo $sidebar_content_font['font-family']; ?>;
            font-size:       <?php echo $sidebar_content_font['font-size'] ?>;
            font-weight:     <?php echo $sidebar_content_font['font-weight'] ?>;
            font-style:      <?php echo $sidebar_content_font['font-style'] ?>;
            text-decoration: <?php echo $sidebar_content_font['text-decoration'] ?>;
            text-transform:  <?php echo $sidebar_content_font['text-transform'] ?>;
        }
        .widget-area .widget-title {}

<?php
echo ob_get_clean();