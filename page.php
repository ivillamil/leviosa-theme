<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package leviosa
 */

// Hide Body
$hide_body = get_post_meta( get_the_ID(), 'pp_hide_body', true );
$body_position = get_post_meta( get_the_ID(), 'pp_body_position', true );
if ( !$body_position ) $body_position = ot_get_option( 'body_position', 'center' );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main <?php echo (isset($hide_body[0])) ? $hide_body[0] : ''; ?> <?php echo $body_position; ?>" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

            <?php include('parts/toggle-icon.php'); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
