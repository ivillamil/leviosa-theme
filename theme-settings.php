<?php

add_filter( 'ot_theme_mode', '__return_true' );
add_filter( 'ot_show_pages', '__return_false' );
add_filter( 'ot_show_new_layout', '__return_false' );

load_template( trailingslashit( get_template_directory() ) . 'options/ot-loader.php' );
load_template( trailingslashit( get_template_directory() ) . 'inc/theme-options.php' );
load_template( trailingslashit( get_template_directory() ) . 'inc/meta-boxes.php' );


add_action( 'admin_menu', 'leviosa_custom_menu_page' );
function leviosa_custom_menu_page(){
    add_menu_page(
        $page_title = __( 'Leviosa Settings', 'leviosa' ),
        $menu_title = __( 'Leviosa', 'leviosa' ),
        $capability = 'manage_options',
        $menu_slug  = 'leviosa_settings',
        $function   = '',
        $icon_url   = 'none',
        $position   = 99);
}

add_filter( 'ot_theme_options_parent_slug', 'leviosa_ot_theme_options_parent_slug' );
function leviosa_ot_theme_options_parent_slug() {
    return 'leviosa_settings';
}
add_filter( 'ot_theme_options_menu_slug', 'leviosa_ot_theme_options_menu_slug' );
function leviosa_ot_theme_options_menu_slug() {
    return 'leviosa_settings';
}
add_filter( 'ot_theme_options_page_title', 'leviosa_ot_theme_options_page_title' );
function leviosa_ot_theme_options_page_title() {
    return __( 'Leviosa Theme Options', 'leviosa' );
}

add_action( 'admin_enqueue_scripts', 'leviosa_admin_styles' );
function leviosa_admin_styles() {
    global $current_screen;

    wp_enqueue_style( 'font-awesome' );
    wp_enqueue_style( 'leviosa-admin-style', get_template_directory_uri() . '/css/theme-admin.css' );

    $plugins = array(
        'layerSlider' => 'no',
        'widgetkit'   => 'no',
        'logo'        => false,
        'logo_left'   => ot_get_option( 'logo_left', 0 ),
        'logo_size'   => ot_get_option( 'logo_size', 80 ),
        'logo_top'    => ot_get_option( 'logo_top', 0 ),
        'headerBg'    => leviosa_get_rgba( ot_get_option( 'header_color', '#000000' ), (((int) ot_get_option( 'header_bg_opacity', 80 )) / 100) )
    );

    if ( is_plugin_active( 'LayerSlider/layerslider.php' ) )
        $plugins['layerSlider'] = 'si';

    if ( is_plugin_active( 'widgetkit/widgetkit.php' ) )
        $plugins['widgetkit']   = 'si';

    if ( $logo = ot_get_option( 'logo', false ) )
        $plugins['logo'] = $logo;



    wp_register_script( 'ot-conditionals-js', get_template_directory_uri() . '/js/ot-conditionals.js', array('jquery', 'backbone'), null, true );
    wp_enqueue_script ( 'ot-conditionals-js' );
    wp_localize_script( 'ot-conditionals-js', 'levSettings', $plugins );

    wp_enqueue_script( 'ot-logo-settings-js', get_template_directory_uri() . '/js/ot-logo-settings.js', array('jquery', 'backbone', 'underscore'), null, true );

    if ( $current_screen && ( $current_screen->base == 'edit' || $current_screen->base == 'post' ) ) {
        wp_enqueue_script( 'ot-mb-conditionals-js', get_template_directory_uri() . '/js/ot-mb-conditionals.js', array('jquery', 'backbone'), null, true );
    }

}

/**
 * Login Enqueue Script
 */
add_action( 'login_enqueue_scripts', 'leviosa_login_styles' );
function leviosa_login_styles() {
    $login_logo = ot_get_option( 'login_logo', false );
    $login_bg   = ot_get_option( 'login_background', false );

    if ( ! $login_logo )
        return;
    ?>
    <style type="text/css">
        .login h1 a {
            background: url(<?php echo $login_logo ?>) no-repeat center bottom;
            background-size: contain;
            height: 200px;
            margin-top: -50px;
            width: 100%;
        }
    </style>
    <?php

    if ( $login_bg ) :
    ?>
    <style type="text/css">
        body.login {
            background-color:      <?php echo $login_bg['background-color']; ?>;
            background-repeat:     <?php echo $login_bg['background-repeat']; ?>;
            background-attachment: <?php echo $login_bg['background-attachment']; ?>;
            background-position:   <?php echo $login_bg['background-position']; ?>;
            background-image: url( <?php echo $login_bg['background-image']; ?> );
        }
    </style>
    <?php endif;
}


/**
 * Templates
 */
add_action( 'admin_footer', 'leviosa_html_templates' );
function leviosa_html_templates() {

    // Logo position settings
    ?>
    <script id="logoPositionSettings" type="text/template">
        <div id="logo-position-settings">
            <div class="preview"><div class="header"><div class="img-wrapper"><img src="<%= imgSrc %>"></div></div></div>
            <div class="settings">
                <ul>
                    <li>
                        <span><?php _e( 'Size', THEME_SLUG ) ?></span>
                        <span id="logo-size-settings-control"></span>
                    </li>
                    <li>
                        <span><?php _e( 'Top', THEME_SLUG ) ?></span>
                        <span id="logo-top-settings-control"></span>
                    </li>
                    <li>
                        <span><?php _e( 'Left', THEME_SLUG ) ?></span>
                        <span id="logo-left-settings-control"></span>
                    </li>
                </ul>
            </div>
        </div>
    </script>
    <?php
}

/**
 * Filter the options for the body position
 */
function leviosa_filter_body_layout( $array, $field_id ) {

    /* only run the filter where the field ID is my_radio_images */
    if ( $field_id == 'body_position' ) {
        $array = array(
            array(
                'value'   => 'left',
                'label'   => __( 'Left', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/left.png'
            ),
            array(
                'value'   => 'center',
                'label'   => __( 'Center', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/center.png'
            ),
            array(
                'value'   => 'right',
                'label'   => __( 'Right', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/right.png'
            )
        );
    }

    return $array;

}
add_filter( 'ot_radio_images', 'leviosa_filter_body_layout', 10, 2 );

/**
 * Filter the options for the sidebar position
 */
function leviosa_filter_sidebar_layout( $array, $field_id ) {
    if ( $field_id == 'general_layout' ) {
        $array = array(
            array(
                'value'   => 'left-sidebar',
                'label'   => __( 'Left Sidebar', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/left-sidebar.png'
            ),
            array(
                'value'   => 'right-sidebar',
                'label'   => __( 'Right Sidebar', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/right-sidebar.png'
            )
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_sidebar_layout', 10, 2 );

/**
 * Filter the options for the woocommerce layout
 */
function leviosa_filter_shop_layout( $array, $field_id ) {
    if ( $field_id == 'shop_layout' ) {
        $array = array(
            array(
                'value'   => 'four',
                'label'   => __( 'Four Columns', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/four.png'
            ),
            array(
                'value'   => 'three',
                'label'   => __( 'Three Columns', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/three.png'
            ),
            array(
                'value'   => 'two',
                'label'   => __( 'Two Columns', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/two.png'
            ),
            array(
                'value'   => 'one',
                'label'   => __( 'One Column', 'leviosa' ),
                'src'     => get_template_directory_uri() . '/img/layout/one.png'
            )
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_shop_layout', 10, 2 );

/**
 * Filter the options for contact layout
 */
function leviosa_filter_contact_layout( $array, $field_id ) {
    if ( $field_id == 'contact_template_layout' ) {
        $array = array(
            array(
                'value'    => 'two',
                'label'    => __( 'Two Columns', 'leviosa' ),
                'src'      => get_template_directory_uri() . '/img/layout/two.png'
            ),
            array(
                'value'    => 'one',
                'label'    => __( 'One Column', 'leviosa' ),
                'src'      => get_template_directory_uri() . '/img/layout/one.png'
            )
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_contact_layout', 10, 2 );

/**
 * Filter the options for gif loaders
 */
function leviosa_filter_gif_loaders( $array, $field_id ) {
    if ( $field_id == 'gif_ajax_loader' ) {
        $array = array(
            array(
                'value' => 'loading_13_w',
                'label' => __( 'Loading 13 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_13_w.gif'
            ),
            array(
                'value' => 'loading_3_W',
                'label' => __( 'Loading 3 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_3_w.gif'
            ),
            array(
                'value' => 'loading_1_w',
                'label' => __( 'Loading 1 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_1_w.gif'
            ),
            array(
                'value' => 'loading_4_w',
                'label' => __( 'Loading 4 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_4_w.gif'
            ),
            array(
                'value' => 'loading_5_w',
                'label' => __( 'Loading 5 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_5_w.gif'
            ),
            array(
                'value' => 'loading_7_w',
                'label' => __( 'Loading 7 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_7_w.gif'
            ),
            array(
                'value' => 'loading_8_w',
                'label' => __( 'Loading 8 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_8_w.gif'
            ),
            array(
                'value' => 'loading_10_w',
                'label' => __( 'Loading 10 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_10_w.gif'
            ),
            array(
                'value' => 'loading_9_w',
                'label' => __( 'Loading 9 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_9_w.gif'
            ),
            array(
                'value' => 'loading_11_w',
                'label' => __( 'Loading 11 Light', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_11_w.gif'
            ),
            array(
                'value' => 'loading_0_w',
                'label' => __( 'Loading 0 Light', THEME_SLUG ),
                'src'   => ''
            ),

            // Dark
            array(
                'value' => 'loading_13_b',
                'label' => __( 'Loading 13 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_13_b.gif'
            ),
            array(
                'value' => 'loading_3_b',
                'label' => __( 'Loading 3 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_3_b.gif'
            ),
            array(
                'value' => 'loading_1_b',
                'label' => __( 'Loading 1 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_1_b.gif'
            ),
            array(
                'value' => 'loading_4_b',
                'label' => __( 'Loading 4 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_4_b.gif'
            ),
            array(
                'value' => 'loading_5_b',
                'label' => __( 'Loading 5 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_5_b.gif'
            ),
            array(
                'value' => 'loading_7_b',
                'label' => __( 'Loading 7 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_7_b.gif'
            ),
            array(
                'value' => 'loading_8_b',
                'label' => __( 'Loading 8 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_8_b.gif'
            ),
            array(
                'value' => 'loading_10_b',
                'label' => __( 'Loading 10 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_10_b.gif'
            ),
            array(
                'value' => 'loading_9_b',
                'label' => __( 'Loading 9 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_9_b.gif'
            ),
            array(
                'value' => 'loading_11_b',
                'label' => __( 'Loading 11 Dark', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/ajax/loading_11_b.gif'
            ),
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_gif_loaders', 10, 2 );

/**
 * Filter options for animation direction
 */
function leviosa_filter_input_animation( $array, $field_id ) {
    if ( $field_id == 'input_animation' || $field_id == 'pp_input_animation' ) {
        $array = array(
            array(
                'value' => 'from_left',
                'label' => __( 'From Left', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/from_left.png'
            ),
            array(
                'value' => 'from_top',
                'label' => __( 'From Top', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/from_top.png'
            ),
            array(
                'value' => 'from_right',
                'label' => __( 'From Right', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/from_right.png'
            ),
            array(
                'value' => 'from_bottom',
                'label' => __( 'From Bottom', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/from_bottom.png'
            )
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_input_animation', 100, 2 );

/**
 * Filter options for the output animation
 */
function leviosa_filter_output_animation( $array, $field_id ) {
    if ( $field_id == 'output_animation' || $field_id == 'pp_output_animation' ) {
        $array = array(
            array(
                'value' => 'to_left',
                'label' => __( 'To Left', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/to_left.png'
            ),
            array(
                'value' => 'to_top',
                'label' => __( 'To Top', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/to_top.png'
            ),
            array(
                'value' => 'to_right',
                'label' => __( 'To Right', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/to_right.png'
            ),
            array(
                'value' => 'to_bottom',
                'label' => __( 'To Bottom', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/directions/to_bottom.png'
            )
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_output_animation', 100, 2 );

/**
 * Filter the options for the slider controls
 */
function leviosa_filter_slider_controls( $array, $field_id ) {
    if ( $field_id == 'slider_controls_style' ) {
        $array = array(
            array(
                'value' => 'arrow',
                'label' => __( 'Arrow', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/arrow.png'
            ),
            array(
                'value' => 'circle_arrow',
                'label' => __( 'Circle Arrow', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/circle_arrow.png'
            ),
            array(
                'value' => 'circle_rounded',
                'label' => __( 'Circle Rounded', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/circle_rounded.png'
            ),
            array(
                'value' => 'double_thin',
                'label' => __( 'Double Thin', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/double_thin.png'
            ),
            array(
                'value' => 'play_controls',
                'label' => __( 'Play Controls', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/play_controls.png'
            ),
            array(
                'value' => 'rounded',
                'label' => __( 'Rounded', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/rounded.png'
            ),
            array(
                'value' => 'thin',
                'label' => __( 'Thin', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/thin.png'
            ),
            array(
                'value' => 'triangle',
                'label' => __( 'Triangle', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/controls/triangle.png'
            )
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_slider_controls', 10, 2 );

/**
 * Filter the options for body position in each page
 */
function leviosa_filter_pp_position( $array, $field_id ) {
    if ( $field_id == 'pp_body_position' ) {
        $array = array(
            array(
                'value' => 'left',
                'label' => __( 'Left', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/layout/left.png'
            ),
            array(
                'value' => 'center',
                'label' => __( 'Center', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/layout/center.png'
            ),
            array(
                'value' => 'right',
                'label' => __( 'Right', THEME_SLUG ),
                'src'   => get_template_directory_uri() . '/img/layout/right.png'
            )
        );
    }

    return $array;
}
add_filter( 'ot_radio_images', 'leviosa_filter_pp_position', 10, 2 );

/**
 * Filter typography fields
 */
function leviosa_filter_typography_fields( $array, $field_id ) {
    return array(
        'font-color',
        'font-family',
        'font-size',
        'font-style',
        'font-weight',
        'text-decoration',
        'text-transform'
    );
}
add_filter( 'ot_recognized_typography_fields', 'leviosa_filter_typography_fields', 10, 2 );

/**
 * Filter the opacity units
 */
function leviosa_filter_measurement_unit_types( $array, $field_id ) {

    /* only run the filter on measurement with a field ID of my_measurement */
    if ( $field_id == 'body_opacity' ) {
        $array = array( 'per' => '%' );
    }

    return $array;
}
add_filter( 'ot_measurement_unit_types', 'leviosa_filter_measurement_unit_types', 10, 2 );


/**
 * Filter Main content font size
 */
function leviosa_filter_main_fontsize_units( $array, $field_id ) {
    if ( $field_id == 'main_font_size' ) {
        $array = array(
            'px' => 'px'
        );
    }

    return $array;
}
add_filter( 'ot_measurement_unit_types', 'leviosa_filter_main_fontsize_units', 10, 2 );

/**
 * Filter Fonts Family
 */
function leviosa_filter_ot_recognized_font_families( $array, $field_id ) {
    //$array         = array();
    $option_fonts  = get_option( 'leviosa_google_fonts' );
    $array_options = unserialize( $option_fonts );

    foreach( $array_options as $key => $val ) {
        $array[$key] = $val['title'];
    }

    asort( $array );
    return $array;

}
add_filter( 'ot_recognized_font_families', 'leviosa_filter_ot_recognized_font_families', 10, 2 );