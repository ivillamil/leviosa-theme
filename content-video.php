<?php
$video_link       = get_post_meta( get_the_ID(),   'lv_video_link',   true );
$video_code       = get_post_meta( get_the_ID(),   'lv_embed_video',  true );
$video_width      = get_post_meta( get_the_ID(),   'lv_video_width',  true );
$video_height     = get_post_meta( get_the_ID(),   'lv_video_height', true );
$video_player     = get_post_meta( get_the_ID(),   'lv_video_player', true );
$video_fullscreen = (in_array( 'fullscreen', $video_player )) ? 'true' : 'false';
$video_controls   = (in_array( 'controls', $video_player )) ? 'true' : 'false';

if ( strlen( trim( $video_width ) ) == 0 )
    $video_width = 560;

if ( strlen( trim( $video_height ) ) == 0 )
    $video_height = 315;

if ( isset( $video_link ) && strlen( trim( $video_code ) ) == 0 )
    $video_src = leviosa_get_video_src( $video_link );
?>

<article id="post-<?php the_ID() ?>" <?php post_class( 'space-bottom-3  ' ) ?>>
    <?php if( strlen( trim( $video_code ) ) !== 0 ) : ?>
    <div class="entry-content">
        <div class="video-wrapper">
            <?php
            $video_code = preg_replace('/width="(\d+)"/', 'width="' . $video_width . '"', $video_code);
            $video_code = preg_replace('/height="(\d+)"/', 'height="' . $video_height . '"', $video_code);
            echo $video_code
            ?>
        </div>
    </div>
    <?php else : ?>
    <div class="entry-content">
        <div class="video-wrapper">
            <?php echo do_shortcode( '[leviosa_video from="' . esc_attr( $video_src['from'] ) . '" id="' . esc_attr( $video_src['id'] ) . '" width="' . $video_width . '" height="' . $video_height . '" fullscreen="' . $video_fullscreen . '" controls="' . $video_controls . '"]' ) ?>
        </div>
    </div>
    <?php endif; ?>

    <header class="entry-header">
        <h1 class="entry-title">
            <span class="icon-wrapper"><i class="levicon-facetime-video"></i></span>
            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h1>

        <?php if ( 'post' == get_post_type() ) : ?>
        <div class="entry-meta"><?php leviosa_posted_on(); ?></div>

        <div class="entry-meta">
            <?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list( __( ', ', 'leviosa' ) );
                if ( $categories_list && leviosa_categorized_blog() ) :
                    ?>
                    <span class="cat-links">
                    <?php printf( __( '| %1$s', 'leviosa' ), $categories_list ); ?>
                </span>
                <?php endif; // End if categories ?>

                <?php
                /* translators: used between list items, there is a space after the comma */
                $tags_list = get_the_tag_list( '', __( ', ', 'leviosa' ) );
                if ( $tags_list ) :
                    ?>
                    <span class="tags-links">
                    <?php printf( __( '| Tagged %1$s', 'leviosa' ), $tags_list ); ?>
                </span>
                <?php endif; // End if $tags_list ?>

            <?php endif; // End if 'post' == get_post_type() ?>

            <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
                <span class="comments-link">| <?php comments_popup_link( __( 'Leave a comment', 'leviosa' ), __( '1 Comment', 'leviosa' ), __( '% Comments', 'leviosa' ) ); ?></span>
            <?php endif; ?>

            <?php edit_post_link( __( 'Edit', 'leviosa' ), '<span class="edit-link">', '</span>' ); ?>
        </div><!-- .entry-meta -->
        <?php endif; ?>
    </header>


    <?php if( is_single() ) : ?>
    <div class="entry-content"><?php the_content() ?></div>
    <?php else : ?>
    <div class="entry-content"><?php the_excerpt() ?></div>
    <?php endif; ?>

    <?php if ( 'post' == get_post_type() ) : ?>
        <div class="entry-date-meta">
            <?php leviosa_posted_meta(); ?>
        </div><!-- .entry-meta -->
    <?php endif; ?>
</article>