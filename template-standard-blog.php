<?php
/**
 * Template Name: Standard Blog
 *
 * @package leviosa
 */ ?>

<?php get_header(); ?>
<?php query_posts( 'post_type=post' ); ?>

    <div id="primary" class="content-area blog-page blog-standard">
        <main id="main" class="site-main center" role="main">
            <div class="row collapse">
                <div class="one columns">&nbsp;</div>
                <div class="eight columns">
                    <div class="blog-content">
                        <?php if ( have_posts() ) : ?>

                            <?php /* Start the Loop */ ?>
                            <?php while ( have_posts() ) : the_post(); ?>

                                <?php
                                /* Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'content', get_post_format() );
                                ?>

                            <?php endwhile; ?>

                            <?php leviosa_paging_nav(); ?>

                        <?php else : ?>

                            <?php get_template_part( 'content', 'none' ); ?>

                        <?php endif; ?>
                    </div>
                </div>

                <div class="three columns">
                    <div class="blog-sidebar">
                        <?php dynamic_sidebar( 'blog-sidebar' ) ?>
                    </div>
                </div>
            </div>
        </main>
    </div>

<?php get_footer(); ?>