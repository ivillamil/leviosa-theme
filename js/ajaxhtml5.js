var lvApp = lvApp || {};

;(function($){

    lvApp.AjaxNavigation = {
        cache: function() {
            this.$bodyContent    = $('#content');
            this.$footer         = $('#colophon');
            this.$header         = $('#masthead');
            this.$mainNavigation = this.$header.find('#site-navigation');
            this.$page           = $('#page');
            this.$background     = this.$page.find('#bg_slider');
        },

        changeBackgrounds: function(actual, next) {
            next.css('z-index',0);
            actual.css('z-index',1);
            this.$background.append(next);

            actual.fadeOut('slow', function(){
                actual.remove();
            });

        },

        changePages: function(actual, next) {
            var pageIndex = next.find('.hentry').attr('id');
            var self = this;

            this.pageOut(actual).done( function(){ self.pageIn(next) } );
        },

        events: function() {
            this.$mainNavigation.delegate('a', 'click', $.proxy(this.loadPage, this));
            this.$footer.delegate('a', 'click', $.proxy(this.loadPage, this));
            this.$bodyContent.delegate('a', 'click', $.proxy(this.loadPage, this));
            this.$bodyContent.delegate('a#toggle-content', 'click', $.proxy(this.toggleContent, this));

            $(window).on('popstate', $.proxy(this.navigationChanged, this));
        },

        getCurrentBg: function() {
            return this.$page.find('#bg_slider > div');
        },

        getCurrentPage: function() {
            return this.$bodyContent.find('.content-area');
        },

        getLeft: function(page, isCenter) {
            var parent = page.parent();
            if (isCenter)
                return (parent.outerWidth() - page.outerWidth())/2;
            else
                return 0;
        },

        hideLoader: function() {
            $('#ajax-loader-wrapper').remove();
        },

        init: function() {
            this._arrPages = [];
            this._animating = false;
            this._popstate = false;
            this.cache();
            this.setupSettings();
            this.events();
        },

        loadPage: function(e) {
            var el = $(e.target).closest('a');
            var url = el.attr('href');
            var actual = this.getCurrentPage();
            var actualBg = this.getCurrentBg();
            var nextPage, nextBg;
            var urlPat = new RegExp("\/", "g");
            var loc = window.location.href || window.location;

            if( this._animating )
                return false;

            if( undefined !== el.attr('href') && el.attr('href').replace(urlPat, '') === loc.replace(urlPat, '') )
                return false;

            if( undefined !== el.attr('href') && -1 !== el.attr('href').indexOf('wp-admin') )
                return true;

            if( undefined !== el.attr('href') && this._globalSettings.homeUrl.replace(urlPat, '') === el.attr('href').replace(urlPat, '') )
                return true;

            if( undefined !== el.attr('target') && el.attr('target') === '_blank' )
                return true;

            if( el.attr('id') === 'toggle-button' )
                return this.toggleContent(e);

            var $sliderControls = this.$footer.find('.slider-controls');
            if ( undefined !== $sliderControls.html() )
                $sliderControls.remove();

            this.showLoader();
            this._animating = true;

            var res = $.ajax({url: url});
            var self = this;
            var dom = $('<div></div>');
            res.success( function(data){
                self.hideLoader();

                if( el.closest('.main-navigation').length > 0 )
                    if( el.closest('.sub-menu').html() !== undefined )
                        self.unmarkNavigation();
                    else
                        self.updateNavigation(el);
                else
                    self.unmarkNavigation();

                if(self.$mainNavigation.hasClass('toggled')) {
                    self.$mainNavigation.removeClass('toggled');
                }

                dom.html(data);
                self._nextPageSettings = JSON.parse(dom.find('#page').attr('data-settings'));
                nextPage = dom.find('.content-area');
                nextBg = dom.find('#bg_slider > div');

                if ( self.supportsHistory() )
                    history.pushState({pushState:true},null,url);

                self.changeBackgrounds( actualBg, nextBg );
                self.changePages( actual, nextPage );
            } );

            return false;
        },

        maximizePage: function(page) {
            var width = page.attr('data-width');
            var height = page.attr('data-height');
            var left = page.attr('data-left');

            page.removeClass('minimized');

            page.css({
                'width': width,
                'height': height,
                'margin-left': left + 'px'
            });

        },

        minimizePage: function(page) {
            var width = page.outerWidth();
            var height = page.outerHeight();
            var isCenter = page.hasClass('center');
            var left = this.getLeft(page, isCenter);
            var buttonSize = 34;

            page.attr('data-width', width);
            page.attr('data-height', height);
            page.attr('data-left', left);

            page.css({
                'width': width,
                'height': height,
                'margin-left': left + 'px'
            });

            page.addClass('minimized');

            setTimeout(function(){
                page.css({
                    'width': buttonSize,
                    'height': buttonSize,
                    'margin-left': width - buttonSize
                });

                if(isCenter)
                    page.css('margin-left', width - buttonSize + left);
                else
                    page.css('margin-left', width - buttonSize);

            },100);

        },

        navigationChanged: function(e) {
            var url = window.location.href || window.location;
            var actual = this.getCurrentPage();
            var actualBg = this.getCurrentBg();
            var nextPage, nextBg, el;
            var urlPat = new RegExp("\/", "g");

            if (!this._popstate) {
                this._popstate = true;
                return false;
            }

            if (url.replace(urlPat, '') === this._globalSettings.homeUrl.replace(urlPat, '')) {
                window.location = url;
                return true;
            }

            this.showLoader();
            this._animating = true;

            var res = $.ajax({url: url});
            var self = this;
            var dom = $('<div></div>');
            res.success( function(data){
                self.hideLoader();

                self.$mainNavigation.find('a').each(function(i,link){
                    if(link.getAttribute('href').replace(urlPat, '') === url.replace(urlPat, ''))
                        el = $(link);
                });

                if(el.html() !== undefined)
                    self.updateNavigation(el);
                else
                    self.unmarkNavigation();

                dom.html(data);
                self._nextPageSettings = JSON.parse(dom.find('#page').attr('data-settings'));
                nextPage = dom.find('.content-area');
                nextBg = dom.find('#bg_slider > div');

                self.changeBackgrounds( actualBg, nextBg );
                self.changePages( actual, nextPage );
            } );

            return false;
        },

        pageIn: function(page) {
            var animation = this._globalSettings.animationFrom;
            var speed = parseInt(this._globalSettings.animationSpeed) + 100;
            var self = this;

            if (this._nextPageSettings.input_animation !== undefined
                && this._nextPageSettings.input_animation.length > 0
                && this._nextPageSettings.input_animation !== this._globalSettings.animationFrom)
                animation = this._nextPageSettings.input_animation;

            page.addClass(animation);

            this.$bodyContent.html(page);

            setTimeout(function(){
                page.removeClass(animation);
                self._actualPageSettings = self._nextPageSettings;
                self._nextPageSettings = {};
                self._animating = false;
            }, speed);
        },

        pageOut: function(page) {
            var dfd = $.Deferred();
            var animation = this._globalSettings.animationTo;
            var speed = parseInt(this._globalSettings.animationSpeed) + 100;

            if (this._actualPageSettings.output_animation !== undefined
                && this._actualPageSettings.output_animation.length > 0
                && this._actualPageSettings.output_animation !== this._globalSettings.animationTo)
                animation = this._actualPageSettings.output_animation;

            page.addClass(animation);

            setTimeout(function(){
                dfd.resolve();
            }, speed);

            return dfd.promise();
        },

        setupSettings: function() {
            this._actualPageSettings   = JSON.parse( this.$page.attr('data-settings') );
            this._nextPageSettings     = {};
            this._globalSettings       = ajaxSettings;
        },

        showLoader: function() {
            if ($('#ajax-loader-wrapper').html() !== undefined)
                return;

            var img = $('<img>').attr('src', this._globalSettings.loadersUri + this._globalSettings.gifAjaxLoader + '.gif');
            var wrapper = $('<div></div>')
                .attr('id', 'ajax-loader-wrapper')
                .append(img)
                .appendTo($('body'));

            wrapper.addClass(this._globalSettings.gifAjaxLoader);

            img.on('load', function(){
                wrapper.css({
                    'margin-left': - img.width() + 'px',
                    'margin-top': - img.height() + 'px',
                    'opacity': 1
                });
            });
        },

        supportsHistory: function() {
            return !!(window.history && history.pushState);
        },

        toggleContent: function(e) {

            if( this._animating )
                return false;

            var actualPage = this.getCurrentPage().find('.site-main');
            var self = this;

            this._animating = true;

            if (!actualPage.hasClass('minimized'))
                this.minimizePage( actualPage );
            else
                this.maximizePage( actualPage );

            setTimeout(function(){ self._animating = false; }, 500);

            return false;
        },

        unmarkNavigation: function() {
            this.$mainNavigation.find('li')
                .removeClass('current-menu-item')
                .removeClass('current_page_item');
        },

        updateNavigation: function(target) {
            target.closest('ul')
                .find('li')
                .removeClass('current-menu-item')
                .removeClass('current_page_item');
            target.closest('li').addClass('current-menu-item current_page_item');
        }
    };

    lvApp.AjaxNavigation.init();

})(jQuery);