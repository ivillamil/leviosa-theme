var lvApp = lvApp || {};

;(function($){

    lvApp.LayerSliderControls = {
        cache: function() {
            this.$slider = $('div[id*="layerslider_"]');
            this.$controls = $('.slider-controls');
        },

        events: function() {
            this.$controls.delegate('a.prev', 'click', $.proxy(this.goPrev, this));
            this.$controls.delegate('a.next', 'click', $.proxy(this.goNext, this));
        },

        goNext: function() {
            this.$slider.layerSlider('next');
            return false;
        },

        goPrev: function() {
            this.$slider.layerSlider('prev');
            return false;
        },

        init: function() {
            this.cache();
            this.events();
        }
    };

    lvApp.LayerSliderControls.init();

})(jQuery);