var lvApp = lvApp || {};

;(function($, app, Bb, settings) {

    var OTMetabox = Bb.View.extend({
        el: '#wpbody-content',

        events: {
            "change input[name=post_format]": "formatSelected",
            "change input[name=post_title]": "changeTitle",
            "change #link_mb input[name=lv_link_title]": "changeLinkTitle",
            "change input[name=lv_video_width]": "updateVideoSize",
            "change input[name=lv_video_height]": "updateVideoSize",
            "click  input[name='lv_video_proportions[0]']": "saveVideoSize"
        },

        cache: function() {
            this.$screenOptions = this.$('#adv-settings');
            this.$sortablesArea = this.$('#normal-sortables');
            this.$formatsArea   = this.$('#formatdiv');

            this.$linkMb        = this.$('#link_mb');
            this.$quoteMb       = this.$('#quote_mb');
            this.$videoMb       = this.$('#video_mb');
            this.$featuredImage = this.$('#postimagediv');

            //Video controls
            this.$videoSwitcher = this.$videoMb.find('input[name="lv_video_proportions[0]"]');
            this.$videoWidth    = this.$videoMb.find('input[name="lv_video_width"]');
            this.$videoHeight   = this.$videoMb.find('input[name="lv_video_height"]');

            //options
            this.$options       = [
                { "option": "image", "dom": this.$screenOptions.find('input[name="postimagediv-hide"]') },
                { "option": "link",  "dom": this.$screenOptions.find('input[name="link_mb-hide"]') },
                { "option": "quote", "dom": this.$screenOptions.find('input[name="quote_mb-hide"]') },
                { "option": "video", "dom": this.$screenOptions.find('input[name="video_mb-hide"]') }
            ];
        },

        changeLinkTitle: function(e) {
            $(e.target).attr('data-autoupdate','0');
        },

        changeTitle: function(e) {
            var title = e.target.value;
            var linkTitle = this.$linkMb.find('input[name="lv_link_title"]');

            if (!this.$linkMb.hasClass('closed') && linkTitle.attr('data-autoupdate') === '1') {
                linkTitle.val(title);
            }
        },

        formatSelected: function(e) {
            var option = e.target.value;
            if (option === 0 || option === "0")
                option = 'aside';

            this.setPostFields(option);
        },

        initialize: function() {
            this.cache();
            $(document).on('ready', $.proxy(this.setupFormat, this));
        },

        saveVideoSize: function(e) {
            if (e.target.checked) {
                this.$videoHeight.attr('data-height', this.$videoHeight.val());
                this.$videoWidth.attr('data-width', this.$videoWidth.val());
            }
        },

        setPostFields: function(option) {
            var item, image;

            _.each( this.$options, function(obj) {
                if(obj.dom[0].checked)
                    obj.dom.trigger('click');
            } );

            if(option !== 'aside') {
                item = _.findWhere(this.$options, {"option": option});
                item.dom.trigger('click');
            } else {
                image = _.findWhere(this.$options, {"option": "image"});
                image.dom.trigger('click');
            }
        },

        setupFormat: function() {
            var option;
            var postTitle = this.$('input[name="post_title"]');
            var linkTitle = this.$linkMb.find('input[name="lv_link_title"]');

            this.$formatsArea.find('input').each(function(i,obj) {
                if (obj.checked)
                    option = obj.value;
            });

            if (option === 0 || option === '0')
                option = 'aside';

            this.setPostFields(option);

            if(postTitle.val() === linkTitle.val())
                linkTitle.attr('data-autoupdate', '1');
            else
                linkTitle.attr('data-autoupdate', '0');

            if(this.$videoSwitcher[0].checked) {
                this.$videoHeight.attr('data-height', this.$videoHeight.val());
                this.$videoWidth.attr('data-width', this.$videoWidth.val());
            }
        },

        updateVideoSize: function(e) {
            var el = $(e.target);
            var previousWidth  = this.$videoWidth.attr('data-width');
            var previousHeight = this.$videoHeight.attr('data-height');

            if ( el.attr('name') === 'lv_video_width' && this.$videoSwitcher[0].checked ) {
                this.$videoHeight.val( parseInt( ( el.val() * this.$videoHeight.val() ) / previousWidth ) );
            } else if( el.attr('name') === 'lv_video_height' && this.$videoSwitcher[0].checked ) {
                console.log( parseInt( ( el.val() * this.$videoWidth.val() ) / previousHeight ) );
                this.$videoWidth.val( parseInt( ( el.val() * this.$videoWidth.val() ) / previousHeight ) );
            }
        }
    });

    app.otMetabox = new OTMetabox();

})(jQuery, lvApp, Backbone, levSettings);