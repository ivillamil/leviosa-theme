/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */

var lvApp = lvApp || {};

;(function($){
    lvApp.Navigation = {

        init: function() {
            this.cache();
            this.setupMobileNav();
            this.setupToggle();
        },

        cache: function() {
            this._bgSlider = document.getElementById('bg_slider');
        },

        getLayerSlider: function() {
            var $slider = $(this._bgSlider).find('div[id*="layerslider_"]');
            return $slider;
        },

        setupMobileNav: function() {
            var container, button, menu;

            container = document.getElementById('site-navigation');

            if ( ! container )
                return;

            button = container.getElementsByTagName( 'h1' )[0];
            if ( 'undefined' === typeof button )
                return;

            menu = container.getElementsByTagName( 'ul' )[0];

            // Hide menu toggle button if menu is empty and return early.
            if ( 'undefined' === typeof menu ) {
                button.style.display = 'none';
                return;
            }

            if ( -1 === menu.className.indexOf( 'nav-menu' ) )
                menu.className += ' nav-menu';

            button.onclick = function() {
                if ( -1 !== container.className.indexOf( 'toggled' ) )
                    container.className = container.className.replace( ' toggled', '' );
                else
                    container.className += ' toggled';
            };
        },

        setupToggle: function() {
            var sidebarToggle, body, slider;
            var self = this;

            sidebarToggle = document.getElementById( 'sidebar-toggle' );
            body = document.body;

            if ( 'undefined' !== typeof sidebarToggle ) {
                sidebarToggle.onclick = function() {
                    //Slider
                    slider = self.getLayerSlider();

                    // Toggle Button
                    if ( -1 !== sidebarToggle.className.indexOf( 'active' ) ) {
                        sidebarToggle.className = sidebarToggle.className.replace(' active', '');
                        ( undefined !== slider.html() ) ? slider.layerSlider('start') : '';
                    } else {
                        sidebarToggle.className += ' active';
                        ( undefined !== slider.html() ) ? slider.layerSlider('stop') : '';
                    }

                    // Body
                    if ( -1 !== body.className.indexOf( 'show_sidebar' ) )
                        body.className = body.className.replace(' show_sidebar', '');
                    else
                        body.className += ' show_sidebar';


                    return false;
                }
            }
        }
    };

    lvApp.Navigation.init();
})(jQuery);
