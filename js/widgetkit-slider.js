var lvApp = lvApp || {};

;(function($){

    lvApp.LayerSliderControls = {
        cache: function() {
            this.$slider = $('#bg_slider div[id*="slideshow-"]');
            this.$controls = $('.slider-controls');
        },

        events: function() {
            this.$controls.delegate('a.prev', 'click', $.proxy(this.goPrev, this));
            this.$controls.delegate('a.next', 'click', $.proxy(this.goNext, this));
        },

        goNext: function() {
            this.$slider.slideshow('nextSlide');
            return false;
        },

        goPrev: function() {
            this.$slider.slideshow('prevSlide');
            return false;
        },

        init: function() {
            this.cache();
            this.events();
        }
    };

    lvApp.LayerSliderControls.init();

})(jQuery);