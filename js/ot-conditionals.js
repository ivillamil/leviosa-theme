var lvApp = lvApp || {};

;(function($, app, Bb, settings){


    var OTreeGeneral = Bb.View.extend({
        el: '#section_general',

        events: {
            "click input[name='option_tree[content_loading]']": "contentLoadingSelected"
        },

        cache: function() {
            this.$contentLoadingNormal       = this.$('#content_loading-1');

            // Ajax
            this.$animationSpeedSettings     = this.$('#setting_animation_speed');
            this.$animationDelaySettings     = this.$('#setting_animation_delay');
            this.$animationInputSettings     = this.$('#setting_input_animation');
            this.$animationOutputSettings    = this.$('#setting_output_animation');
            this.$ajaxLoaderSettings         = this.$('#setting_gif_ajax_loader');
            this.$toggleContentSettings      = this.$('#setting_toggle_content');
        },

        initialize: function() {
            this.cache();

            if ( this.$contentLoadingNormal[0].checked === true ) {
                this.hideAjaxSettings();
            }
        },

        contentLoadingSelected: function(e) {
            if (e.target.value === 'normal') {
                this.hideAjaxSettings();
            } else {
                this.showAjaxSettings();
            }
        },

        hideAjaxSettings: function() {
            this.$animationSpeedSettings.hide();
            this.$animationDelaySettings.hide();
            this.$animationOutputSettings.hide();
            this.$animationInputSettings.hide();
            this.$ajaxLoaderSettings.hide();
            this.$toggleContentSettings.hide();
        },

        showAjaxSettings: function() {
            this.$animationSpeedSettings.show();
            this.$animationDelaySettings.show();
            this.$animationOutputSettings.show();
            this.$animationInputSettings.show();
            this.$ajaxLoaderSettings.show();
            this.$toggleContentSettings.show();
        }
    });

    var OTreeHome = Backbone.View.extend({
        el: '#section_home',
        events: {
            "change select[name='option_tree[general_slider_type]']": "generalSliderSelected",
            "click input[name='option_tree[slider_controls][0]']": "sliderControlsSelected"
        },

        cache: function() {
            // Sliders
            this.$generalSliderSelect        = this.$('#general_slider_type');
            this.$generalSliderSettings      = this.$('#setting_general_slider_type');
            this.$leviosaStaticSettings      = this.$('#setting_leviosa_static_bg');
            this.$leviosaSliderSettings      = this.$('#setting_leviosa_slider_items');
            this.$layerSliderSettings        = this.$('#setting_layer_slider_id');
            this.$widgetkitSliderSettings    = this.$('#setting_widgetkit_slider_id');

            // Slider Controls
            this.$sliderControlsRadio        = this.$('#slider_controls-0');
            this.$sliderControlsStyleSettings= this.$('#setting_slider_controls_style');
        },

        generalSliderSelected: function(e) {
            this.showSliderOption(e.target.value);
        },

        initialize: function() {
            this.cache();

            /* =Hide the Layer Slider option if the plugin is not active
             ------------------------------------------------------------------------------ */
            if( settings.layerSlider === "no" ) {
                this.$generalSliderSelect.find('option[value=gs_layer_slider]').remove();
            }

            /* =Hide the Widgetkit option if the plugin is not active
             ------------------------------------------------------------------------------ */
            if ( settings.widgetkit === "no" ) {
                this.$generalSliderSelect.find('option[value=gs_widgetkit_slider]').remove();
            }


            this.$generalSliderSettings.show();
            this.showSliderOption( this.$generalSliderSelect.val() );

            if ( ! this.$sliderControlsRadio[0].checked ) {
                this.$sliderControlsStyleSettings.hide();
            }
        },

        showSliderOption: function( op ) {
            switch( op ) {
                case 'gs_static_background':
                    this.$leviosaStaticSettings.show();
                    this.$layerSliderSettings.hide();
                    this.$widgetkitSliderSettings.hide();
                    this.$leviosaSliderSettings.hide();
                    break;
                case 'gs_leviosa_slider':
                    this.$leviosaStaticSettings.hide();
                    this.$layerSliderSettings.hide();
                    this.$widgetkitSliderSettings.hide();
                    this.$leviosaSliderSettings.show();
                    break;
                case 'gs_layer_slider':
                    this.$leviosaStaticSettings.hide();
                    this.$layerSliderSettings.show();
                    this.$widgetkitSliderSettings.hide();
                    this.$leviosaSliderSettings.hide();
                    break;
                case 'gs_widgetkit_slider':
                    this.$leviosaStaticSettings.hide();
                    this.$layerSliderSettings.hide();
                    this.$widgetkitSliderSettings.show();
                    this.$leviosaSliderSettings.hide();
                    break;
            }
        },

        sliderControlsSelected: function(e) {
            if (e.target.checked )
                this.$sliderControlsStyleSettings.show();
            else
                this.$sliderControlsStyleSettings.hide();
        }
    });


    if ( $('#page-ot_theme_options').html() !== undefined ) {
        app.otreeGeneral = new OTreeGeneral();
        app.otreeHome = new OTreeHome();
    }


})(jQuery, lvApp, Backbone, levSettings);