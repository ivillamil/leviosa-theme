var lvApp = lvApp || {};
;(function($, app, Bb, settings, _) {

    var LogoSettingsView = Bb.View.extend({
        el: '#section_branding',

        templateSettings: _.template($('#logoPositionSettings').html()),

        events: {},

        adjust_left: function( value ) {
            this.$logo.parent().css('margin-left', value + 'px');
        },

        adjust_size: function( value ) {
            this.$logo.css('width', value + '%');
        },

        adjust_top: function( value ) {
            this.$logo.parent().css('margin-top', value + 'px');
        },

        cache: function() {
            this.$logoPositionArea = this.$('#setting_logo_position');
            this.$logoSizeArea     = this.$('#setting_logo_size');
            this.$logoTopArea      = this.$('#setting_logo_top');
            this.$logoLeftArea     = this.$('#setting_logo_left');

            this.$logoSizeControl  = this.$logoSizeArea.find('.type-numeric-slider').detach();
            this.$logoTopControl   = this.$logoTopArea.find('.type-numeric-slider').detach();
            this.$logoLeftControl  = this.$logoLeftArea.find('.type-numeric-slider').detach();
        },

        initialize: function() {
            this.cache();
            this.reArrange();

            if ( ! settings.logo )
                this.$logoPositionArea.hide();

            this.initSliders();
        },

        initSliders: function() {
            var self = this;
            $(document).on('ready', function(){
                self.$(".ot-numeric-slider-wrap").each(function() {
                    var hidden = $(".ot-numeric-slider-hidden-input", this),
                        value  = hidden.val(),
                        helper = $(".ot-numeric-slider-helper-input", this);
                    if ( ! value ) {
                        value = hidden.data("min");
                        helper.val(value)
                    }

                    $(".ot-numeric-slider", this).slider({
                        min: hidden.data("min"),
                        max: hidden.data("max"),
                        step: hidden.data("step"),
                        value: value,
                        slide: function(event, ui) {
                            hidden.add(helper).val(ui.value);
                            var parent = $(ui.handle).parent().attr('id');
                            self[ 'adjust_' + parent.split('_')[parent.split('_').length - 1] ]( ui.value );
                        }
                    });
                });
            });
        },

        reArrange: function() {
            var controls = this.$logoPositionArea.find('.format-setting').html( this.templateSettings( {imgSrc: settings.logo} ) );
            this.$logoSizeArea.remove();
            this.$logoTopArea.remove();
            this.$logoLeftArea.remove();

            controls.find('#logo-size-settings-control').html(this.$logoSizeControl);
            controls.find('#logo-top-settings-control').html(this.$logoTopControl);
            controls.find('#logo-left-settings-control').html(this.$logoLeftControl);

            controls.find('.preview .header').css('background', settings.headerBg);

            this.$logo = controls.find('.header img');
            this.$logo.css('width', settings.logo_size + '%');
            this.$logo.parent().css({
                'margin-top': settings.logo_top + 'px',
                'margin-left': settings.logo_left + 'px'
            });
        }
    });

    if ( $('#setting_logo_position').html() !== undefined )
        app.logoSettingsView = new LogoSettingsView();

})(jQuery, lvApp, Backbone, levSettings, _);