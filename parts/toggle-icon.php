<?php
$toggle_content   = ot_get_option( 'toggle_content' );
$toggle_content   = ( empty( $toggle_content ) ) ? 'hide' : 'show';
?>
<a id="toggle-button" class="<?php echo $toggle_content ?>">
    <i class="levicon-minus minus"></i>
    <i class="levicon-plus plus"></i>
</a>